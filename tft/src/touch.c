/*
 * touch.c
 *
 *  Created on: 1/11/2015
 *      Author: ademski
 */
#include "TFT.h"
#include "chip.h"


touch_t Touch_Get ( void )  // Falta Terminar, es solo un copipaste, falta ReadWrite!!!
{
	touch_t touchxy;
	touchxy.x=0;
	touchxy.y =0;

	uint8_t txBuf[]={0x90,0},rxBuf[2]={0,0};
	Chip_SSP_DATA_SETUP_T data;

	Chip_GPIO_SetPinOutLow(LPC_GPIO, TOUCH_E);

	data.rx_data=rxBuf;
	data.tx_data=txBuf;

	txBuf[0]=0x94;
	data.length=1;
	data.rx_cnt = 0;
	data.tx_cnt = 0;
	Chip_SSP_RWFrames_Blocking(LPC_SSP0,&data);

	txBuf[0]=0x00;
	data.length=2;
	data.rx_cnt = 0;
	data.tx_cnt = 0;
	Chip_SSP_RWFrames_Blocking(LPC_SSP0, &data);


	touchxy.x= ((rxBuf[0]<<8)|rxBuf[1])>>3;

	txBuf[0]=0xD4;
	data.length=1;
	data.rx_cnt = 0;
	data.tx_cnt = 0;
	Chip_SSP_RWFrames_Blocking(LPC_SSP0,&data);

	txBuf[0]=0;
	data.rx_data= rxBuf;
	data.length=2;
	data.tx_data=txBuf;
	data.rx_cnt = 0;
	data.tx_cnt = 0;
	Chip_SSP_RWFrames_Blocking(LPC_SSP0,&data);

	touchxy.y= ( (rxBuf[0]<<8)|rxBuf[1])>>3;

	txBuf[0]=0x80;
	data.length=1;
	data.rx_data=rxBuf;
	data.tx_data=txBuf;
	data.rx_cnt = 0;
	data.tx_cnt = 0;

	Chip_SSP_RWFrames_Blocking(LPC_SSP0,&data);

	Chip_GPIO_SetPinOutHigh(LPC_GPIO, TOUCH_E);

	return touchxy;

}


void InitTouch ()
{
	const int pin[]={TOUCH_E};
	Chip_IOCON_PinMux(LPC_IOCON,TOUCH_E,IOCON_MODE_PULLUP,IOCON_FUNC0);
	Chip_GPIO_SetPinDIROutput(LPC_GPIO,TOUCH_E);
	Chip_IOCON_DisableOD(LPC_IOCON,TOUCH_E);
	GPIO_SetPin_HIGH(LPC_GPIO,TOUCH_E);

	Chip_IOCON_PinMux(LPC_IOCON,TOUCH_BUSY,IOCON_MODE_PULLUP,IOCON_FUNC0);
	Chip_GPIO_SetPinDIRInput(LPC_GPIO,TOUCH_BUSY);
	Chip_IOCON_DisableOD(LPC_IOCON,TOUCH_BUSY);

	Chip_IOCON_PinMux(LPC_IOCON,TOUCH_PENIRQ,IOCON_MODE_PULLUP,IOCON_FUNC0);
	Chip_GPIO_SetPinDIRInput(LPC_GPIO,TOUCH_PENIRQ);
	Chip_IOCON_DisableOD(LPC_IOCON,TOUCH_PENIRQ);

	Chip_IOCON_PinMux(LPC_IOCON,TOUCH_CLK,IOCON_MODE_INACT,IOCON_FUNC2);
	Chip_IOCON_PinMux(LPC_IOCON,TOUCH_MOSI,IOCON_MODE_INACT,IOCON_FUNC2);
	Chip_IOCON_PinMux(LPC_IOCON,TOUCH_MISO,IOCON_MODE_INACT,IOCON_FUNC2);

	Chip_Clock_EnablePeriphClock(SYSCTL_CLOCK_SSP0);
	Chip_SSP_Set_Mode(LPC_SSP0,SSP_MASTER_MODE);
	Chip_SSP_SetFormat(LPC_SSP0,SSP_BITS_8,SSP_FRAMEFORMAT_SPI,SSP_CLOCK_CPHA0_CPOL0);
	Chip_SSP_SetBitRate(LPC_SSP0,1000000 );
	Chip_SSP_Enable(LPC_SSP0);
	Touch_Get();



}


/* Dependencias:
 * - Libreria TFT
 */

#include	"lineas.h"
#include	"TFT.h"
#include	<string.h>

void set_pixel(uint16_t x, uint16_t y, uint16_t color) {
	/* Colorea un píxel en la coordenada (x,y)
	 */
	//TFT_Set_Address(239-y, 319-x, 239-y, 319-x);
	TFT_Set_Address(y, x, y, x);
	TFT_Write_Data(color);
	return;
}

void LineaH(uint16_t x, uint16_t y, uint16_t d, uint16_t color) {
	// Version vieja
	/* Dibuja línea horizontal entre los puntos
	 * (x,y)-(x+d,y)
	 */

	// Version actual
	/* Dibuja línea horizontal
	 * 		d pixeles de ancho
	 * 		entre las posiciones (x,y) - (x+d-1,y)
	 */
	TFT_Set_Address(y, x, y, x+d-1); // Se agrega el -1,
	TFT_Write_Data(color);
	while (--d) {
		LATCH_WR;
	}

	return;
}

void LineaV(uint16_t x, uint16_t y, uint16_t h, uint16_t color) {
	// Version vieja
	/* Dibuja línea vertical entre los puntos
	 * (x,y)-(x,y+h)
	 */

	// Version actual
	/* Dibuja línea vertical
	 * 		h pixeles de alto
	 * 		entre las posiciones (x,y) - (x,y+h-1)
	 */
	uint32_t i;
	TFT_Set_Address(y, x, y+h, x);
	TFT_Write_Data(color);
	while (--h) {
		//LATCH_WR;
		GPIO_SetPin_LOW(LPC_GPIO,TFT_WR);
		for(i=100;!i;i--);
		GPIO_SetPin_HIGH(LPC_GPIO,TFT_WR);
	}

	return;
}

void LineaV2(int16_t x, int16_t y1, int16_t y2, uint16_t color) {
	/* Dibuja línea vertical entre las posiciones (x,y1) - (x+1,y2)
	 */

	/*
	if(y1 >= SCR_HEIGHT) y1 = SCR_HEIGHT-1;
	if(y2 >= SCR_HEIGHT) y2 = SCR_HEIGHT-1;
	if(y1<0) y1=0;
	if(y2<0) y2=0;

	if(x>=SCR_WIDTH) return;

	if(y1 > y2){
		int16_t y=y1;
		y1=y2;
		y2=y;
		LineaV(x+1,y1,1+(y2-y1)/2,color);
		LineaV(x,y1+(y2-y1)/2+1,1+(y2-y1)/2,color);
	}
	else
	{
		LineaV(x,y1,1+(y2-y1)/2,color);
		LineaV(x+1,y1+(y2-y1)/2+1,1+(y2-y1)/2,color);
	}
	return;*/

	if(y1 >= SCR_HEIGHT) return;
	if(y2 >= SCR_HEIGHT) return;
	if(y1<0) return;
	if(y2<0) return;

	if(y1 > y2){
		LineaV(x,y2,1+y1-y2,color);
	}
	else
		LineaV(x,y1,1+y2-y1,color);

}

void LineaOpt(uint16_t x1, uint16_t y1, uint16_t x2, uint16_t y2,
		uint16_t color) {
	/* Dibuja línea entre los puntos (x1,y1)-(x2,y2)
	 * El algoritmo consiste en reducir la línea
	 * diagonal, en varias líneas horizontales o
	 * verticales
	 * (por la forma en que se maneja el display,
	 * resulta más rápido dibujar varias líneas
	 * horizontales/verticales que dibujar
	 * píxel por píxel una diagonal.
	 * Si |x2-x1| < |y2-y1|, se dibujar líneas
	 * verticales. Caso contrario, horizontales.
	 */
	/* Para visualizar mejor el funcionamiento,
	 * se recomienda ver gráfico adjunto en
	 * el informe (página #####)
	 * ##### COMPLETAR RENGLON / HACER GRAFICO EN INFORME ####
	 */

	uint16_t absDX = 0, absDY = 0;
	signed short int dx = 0, dy = 0;
	uint16_t i = 0; // dummy=0;
	uint16_t x = 0, y = 0;

	/* Se calcula el valor absoluto de los incrementos
	 * en x e y
	 */
	x1 > x2 ? (absDX = x1 - x2) : (absDX = x2 - x1);
	y1 > y2 ? (absDY = y1 - y2) : (absDY = y2 - y1);

	/* Si ambos son cero, es un punto
	 */
	if (!(absDX || absDY)) { // x1=x2, y1=y2 >>> dibuja punto
		set_pixel(x1, y1, color);
		return;
	}
	/* El ++ de los incrementos, es una corrección
	 * del algoritmo empleado, que asegura que el dibujo
	 * de la línea incluya los puntos extremos
	 */
	absDX++;
	absDY++;

	if (absDX > absDY) {	// lineas horizontales
		/* Si absDX > absDY, se hacen líneas horizontales
		 */
		if (x1 > x2) {	//swap
			/* Si x1 > x2, se invierten
			 * los puntos. Esto se hace porque
			 * la función LineaH() dibuja de izquierda
			 * a derecha.
			 */
			x = x1;
			y = y1;
			x1 = x2;
			y1 = y2;
			x2 = x;
			y2 = y;
		}
		// incrementos en x e y signados:
		dx = (signed short int) x2 - (signed short int) x1;
		dy = (signed short int) y2 - (signed short int) y1;
		dx++;
		dy < 0 ? (dy--) : (dy++);

		if (dy < 0) {	// (dx > 0) && (dy < 0)
			/* Si dy < 0, la pendiente es negativa.
			 * Se dibujan líneas horizontales a
			 * altura decreciente
			 * (escalera que baja)
			 */
			for (i = 0; i < absDY; i++) {
				LineaH(x1 + i * absDX / absDY, y1 - i,
						(i + 1) * absDX / absDY - i * absDX / absDY - 1, color);
			}
		} else {		// (dx > 0) && (dy >= 0)
			/* Si dy >= 0, la pendiente es positiva (o cero).
			 * Se dibujan líneas horizontales a
			 * altura creciente
			 * (escalera que sube)
			 */
			for (i = 0; i < absDY; i++) {
				LineaH(x1 + i * absDX / absDY, y1 + i,
						(i + 1) * absDX / absDY - i * absDX / absDY - 1, color);
			}
		}
	} else {	//absDY >= absDX
		/* Si absDY > absDX, se hacen líneas verticales
		 */
		if (y1 > y2) {	//swap
			/* Si y1 > y2, se invierten
			 * los puntos. Esto se hace porque
			 * la función LineaV() dibuja de
			 * abajo hacia arriba
			 */
			x = x1;
			y = y1;
			x1 = x2;
			y1 = y2;
			x2 = x;
			y2 = y;
		}
		// incrementos en x e y signados:
		dx = (signed short int) x2 - (signed short int) x1;
		dy = (signed short int) y2 - (signed short int) y1;
		dx < 0 ? dx-- : dx++;
		dy++;

		if (dx < 0) {	// (dy > 0) && (dx < 0)
			/* Si dx < 0, la pendiente es negativa.
			 * Se dibujan líneas verticales con
			 * posición en X creciente
			 * (escalera que baja)
			 */
			for (i = 0; i < absDX; i++) {
				LineaV(x1 - i, y1 + i * absDY / absDX,
						(i + 1) * absDY / absDX - i * absDY / absDX - 1, color);
			}
		} else {		// (dy > 0) && (dx > 0)
			/* Si dx >= 0, la pendiente es positiva (o cero).
			 * Se dibujan líneas verticales con
			 * posición en X decreciente
			 * (escalera que sube)
			 */
			for (i = 0; i < absDX; i++) {
				LineaV(x1 + i, y1 + i * absDY / absDX,
						(i + 1) * absDY / absDX - i * absDY / absDX - 1, color);
			}
		}
	}
	return;
}


//#include	"basicFunctions.h"
//#include	"GPIOConfig.h"
#include	"TFT.h"
#include	<string.h>
//#include	"chip.h"

/* El display se controla por un bus de 16 bits.
 * Para acceder fácilmente, se hizo un array con los
 * números de puerto y pin correspondiente a cada bit.
 */
__IO uint32_t DB [16] [2] ={
		{TFT_DB0} ,
		{TFT_DB1} ,
		{TFT_DB2} ,
		{TFT_DB3} ,
		{TFT_DB4} ,
		{TFT_DB5} ,
		{TFT_DB6} ,
		{TFT_DB7} ,
		{TFT_DB8} ,
		{TFT_DB9} ,
		{TFT_DB10} ,
		{TFT_DB11} ,
		{TFT_DB12} ,
		{TFT_DB13} ,
		{TFT_DB14} ,
		{TFT_DB15}
};
/*
escribir en estos, como estan secuenciados una sola escritura
	#define			TFT_DB0				0,23//LCD_E
	#define			TFT_DB1				0,24//LCD_D4
	#define			TFT_DB2				0,25//EXPANSION5
	#define			TFT_DB3				0,26//EXPANSION10
	#define			TFT_DB4				0,27//LCD_D6*

despues en estos
	#define			TFT_DB5				0,5//LCD_D7
	#define			TFT_DB6				0,6//LCD_RS
	#define			TFT_DB7				0,7//EXPANSION0
	#define			TFT_DB8				0,8//EXPANSION15
	#define			TFT_DB9				0,9//EXPANSION9
	#define			TFT_DB10			0,10//EXPANSION4

y estos
	#define			TFT_DB11			2,0//EXPANSION14
	#define			TFT_DB12			2,1//EXPANSION8
	#define			TFT_DB13			2,2//EXPANSION3
	#define			TFT_DB14			2,3//EXPANSION13
	#define			TFT_DB15			2,4//EXPANSION7

*/
/*
void TFT_Write_Com_Data (uint32_t Com, uint32_t Dat){
	// Envía el comando Com y el dato Dat

	TFT_Write_Com (Com);
	TFT_Write_Data (Dat);

}
*/

void TFT_Write (uint32_t Sth){
	/* Envía comando al display
	 * TFT_RS = 1 para comando, 0 para dato
	 */
	//uint8_t i=0;

	LPC_GPIO[0].CLR=0x0F8007E0;
	LPC_GPIO[2].CLR=0x1F;
	LPC_GPIO[0].SET=(((Sth&0x1F)<<23)|(Sth&0x7E0));
	LPC_GPIO[2].SET=((Sth&0xF800)>>11);

	/*for (i=0;i<16;i++)
		Chip_GPIO_SetPinState(LPC_GPIO,DB[i][0],DB[i][1],((Sth>>i) & 0x01) ? TRUE : FALSE);
	 */
	LATCH_WR;

////////// ASM
	//write_TFT_ASM(Sth);
	return;
}

void TFT_SetData (uint32_t Sth)
{
	LPC_GPIO[0].CLR=0x0F8007E0;
	LPC_GPIO[2].CLR=0x1F;
	LPC_GPIO[0].SET=(((Sth&0x1F)<<23)|(Sth&0x7E0));
	LPC_GPIO[2].SET=((Sth&0xF800)>>11);

	TFT_RS_HIGH;
}

void TFT_Set_Address (uint32_t x1, uint32_t y1, uint32_t x2, uint32_t y2){
	/* Se configura el rectángulo de la pantalla
	 * donde se graficará.
	 * (Ver detalle del funcionamiento de la
	 * pantalla en informe)
	 */
	TFT_Write_Com_Data(0x0044,(x2<<8)|x1);
	TFT_Write_Com_Data(0x0045,y1);
	TFT_Write_Com_Data(0x0046,y2);
	TFT_Write_Com_Data(0x004e,x1);
	TFT_Write_Com_Data(0x004f,y1);
	TFT_Write_Com(0x0022);
}

uint16_t TFT_Color (uint32_t r, uint32_t g, uint32_t b){
	/* Recibe un color en formato RGB (255,255,255)
	 * Devuelve un valor de color, en formato de
	 * 16 bits para ser enviado al display.
	 */
	/* NOTA:
	 * La definición del display es de 16 bits
	 * (R: 5bits, G:6bits, B:5bits).
	 * por lo tanto, convierte [0-255] en [0-31] o [0-63]
	 * COLOR = RRRRRGGG GGGBBBBB
	 */
	return( ((uint16_t)(r & 0xF8) << 8) | ((uint16_t)(g & 0xFC) << 3) | ((uint16_t)(b & 0xF8) >> 2) );
}

void TFT_Three_Lines ( void ){
	/* Dibuja tres franjas de colores
	 * en pantalla. Se usa para testear
	 * display, y no tiene relevancia
	 * el el proyecto.
	 */
	int i=0,j=0;
	TFT_Set_Address(0,0,239,319);	//toda la pantalla
	//for (k=0;k<16;k++)
	//	set_pin(DB[k][0], DB[k][1], LOW);

    for(i=0;i<320;i++){
    	for (j=0;j<240;j++) {
				if (j < 80)
					TFT_Write_Data(TFT_Color(255,0,0));
					//TFT_Send_Color(TFT_Color(255,0,0));
				else if ( j < 160 )
					TFT_Write_Data(TFT_Color(0,255,0));
					//TFT_Send_Color(TFT_Color(0,255,0));
				else if (j >= 160)//a 45.  (j>=160):recto
					TFT_Write_Data(TFT_Color(0,0,255));
					//TFT_Send_Color(TFT_Color(0,0,255));
			}
    }
    return;
}

void TFT_Black_Slow( void ){
	int i=0,j=0;
	TFT_Set_Address(0,0,239,319);	//toda la pantalla
	//for (k=0;k<16;k++)
	//	set_pin(DB[k][0], DB[k][1], LOW);

    for(i=0;i<320;i++){
    	for (j=0;j<240;j++) {
				if (j < 80)
					TFT_Write_Data(TFT_Color(0,0,0));
					//TFT_Send_Color(TFT_Color(255,0,0));
				else if ( j < 160 )
					TFT_Write_Data(TFT_Color(0,0,0));
					//TFT_Send_Color(TFT_Color(0,255,0));
				else if (j >= 160)//a 45.  (j>=160):recto
					TFT_Write_Data(TFT_Color(0,0,0));
					//TFT_Send_Color(TFT_Color(0,0,255));
			}
    }
    return;
}


void TFT_Black( void ){
	/* Borra la pantalla,
	 * con fondo negro.
	 */
	int i=0;
	TFT_Set_Address(0,0,239,319);	//toda la pantalla
	//for (k=0;k<16;k++)
	//	set_pin(DB[k][0], DB[k][1], LOW);

	TFT_Write_Data(TFT_Color(0,0,0));
	i=320*240-1;
	while(i){
		LATCH_WR;
		i--;
	}
	return;
}

void TFT_FastErase(uint32_t color){
	/* Borra toda la pantalla con
	 * color de fondo elegido
	 */
	uint32_t i=0;
	TFT_Set_Address(0,0,239,319);
	//for (i=0;i<16;i++)
	//	set_pin(DB[i][0], DB[i][1], LOW);

	TFT_Write_Data(color);
	i=320*240;
	i--;
	while(i){
		LATCH_WR;
		i--;
	}
	return;
}
void TFT_PrintBig (uint8_t *stg, uint16_t x, uint16_t y, uint16_t color_texto, uint16_t color_fondo){
//void TFT_PrintBig (uint8_t *stg, uint32_t x, uint32_t y, uint32_t color_texto, uint32_t color_fondo){
// 16x16
#ifdef DEBUG_NO_TEXT
	return;
#endif
	/* Impresión de cadenas de texto en el
	 * display.
	 * Tamaño de letra: 16x16 píxels.
	 * Ver detalle de funcionamiento en TFT_PrintSmall()
	 * (funciona de forma similar)
	 */
	uint8_t i=0,j=0,k=0,l=0, len=0;//,*ch;
	uint8_t * Buffer[30]={0};
	uint8_t linea=0;

	//while(stg[i++]);
	//len=i-1;
	len = strlen((char*)stg);

	//TFT_Set_Address( 239-(y+BIG_Y-1) , 319-(x+BIG_X*len-1) , 239-y , 319-x );
	TFT_Set_Address(y , x,  y+BIG_Y-1 , x+BIG_X*len-1);
	while (*stg){
		Buffer[k] = &Big_Font[0] + ((*stg)-'!'+1)*(BIG_Y*2);
		k++; stg++;
	}
	TFT_RS_HIGH;
	for( l=0 ; l<len ; l++ ){
		for( k=0 ; k<2 ; k++ ){
			for( i=0 ; i<8; i++){
				for(j=BIG_Y-1;j<BIG_Y;j--){ // Tambien con do...while()
//				for( j=0 ; j<BIG_Y ; j++ ){
					//if( (*(Bufer[l])) & ( 1<< (7-i) ) ){
					linea = * ( Buffer[len-1-l] + 2*j + (1-k) ) ;
					//linea = 0x01 << (TAM_X-1-i);
					linea &= 0x01 << (i);
					if( linea )
						TFT_Write_Data(color_texto);
					else
						TFT_Write_Data(color_fondo);
	} } } }
	return;
}

void TFT_PrintSmall (uint8_t *stg, uint16_t x, uint16_t y, uint16_t color_texto, uint16_t color_fondo){
//void TFT_PrintSmall (uint32_t *stg, uint32_t x, uint32_t y, uint32_t color_texto, uint32_t color_fondo){
//8x12
#ifdef DEBUG_NO_TEXT
	return;
#endif
	/* Impresión de cadenas de texto en el
	 * display.
	 * Tamaño de letra: 8x12 píxels.
	 */

	uint8_t i=0,j=0,l=0, len=0;
	uint8_t * Buffer[40]={0};
	uint8_t linea=0;

	//while(stg[i++]);
	//len=i-1;
	len = strlen((char*)stg);
	
	if(len>=40 || !len)	//si excede tamaño pantalla o vacio, salir.
		return;
	
	//Configura la posición a partir de la cual se escriben las letras
	//TFT_Set_Address( 239-(y+SMALL_Y-1) , 319-(x+SMALL_X*len-1) , 239-y , 319-x );
	TFT_Set_Address( y , x, y+SMALL_Y-1 , x+SMALL_X*len-1 );
	/* Las letras se forman usando tablas similares a las que usan
	 * los lcds alfanuméricos. (Small_Font[] y Big_Font[])
	 * Ejemplo:
	 * 0 (cero):
	 * --------		0x00
	 * --------		0x00
	 * -###----		0x70
	 * #---#---		0x88
	 * #---#---		0x88
	 * #---#---		0x88
	 * #---#---		0x88
	 * #---#---		0x88
	 * #---#---		0x88
	 * -###----		0x70
	 * --------		0x00
	 * --------		0x00
	 *
	 * Se carga en Buffer[], las posiciones en las cuales están
	 * las letras dentro de la tabla Small_Font[].
	 */
	i=0;
	while(stg[i] && i<40){
		Buffer[i] = &Small_Font[0] + ((stg[i])-'!'+1)*SMALL_Y;
		i++;
	}

	TFT_RS_HIGH;
	
	for( l=0 ; l<len ; l++ ){
		/* Se recorren todas las letras
		 */
		for( i=0 ; i<SMALL_X; i++){
			/* Se recorren las columnas de
			 * cada letra.
			 */
			for(j=SMALL_Y-1;j<SMALL_Y;j--){
//			for( j=0 ; j<SMALL_Y ; j++ ){
				/* Se recorren las filas
				 * de cada letra */
				// se guarda la linea horizontal en linea
				linea = * ( Buffer[len-1-l] + j ) ;
				// se verifica el bit correspondiente a la
				// columna "i"
				linea &= 0x01 << i;
				if( linea )
					TFT_Write_Data(color_texto);
				else
					TFT_Write_Data(color_fondo);
	}	}	}

return;
}



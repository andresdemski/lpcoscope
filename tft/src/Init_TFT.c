#include	"TFT.h"

extern __IO uint32_t DB [16] [2];

void InitTFT(void){

	uint8_t i=0;
	msDelay(200);

	for (i=0;i<16; i++){
		Chip_IOCON_PinMux(LPC_IOCON,DB[i][0],DB[i][1],IOCON_MODE_PULLUP,IOCON_FUNC0);//GPIO con pull-up
		Chip_GPIO_SetPinDIROutput(LPC_GPIO,DB[i][0],DB[i][1]);//son salidas
		Chip_IOCON_DisableOD(LPC_IOCON,DB[i][0],DB[i][1]);//no open drain
		GPIO_SetPin_LOW(LPC_GPIO,DB[i][0],DB[i][1]); //los bajo
	}


	Chip_IOCON_PinMux(LPC_IOCON,TFT_RS,IOCON_MODE_PULLUP,IOCON_FUNC0);
	Chip_GPIO_SetPinDIROutput(LPC_GPIO,TFT_RS);
	Chip_IOCON_DisableOD(LPC_IOCON,TFT_RS);
	GPIO_SetPin_LOW(LPC_GPIO,TFT_RS);

	Chip_IOCON_PinMux(LPC_IOCON,TFT_CS,IOCON_MODE_PULLUP,IOCON_FUNC0);
	Chip_GPIO_SetPinDIROutput(LPC_GPIO,TFT_CS);
	Chip_IOCON_DisableOD(LPC_IOCON,TFT_CS);
	GPIO_SetPin_LOW(LPC_GPIO,TFT_CS);

	Chip_IOCON_PinMux(LPC_IOCON,TFT_RESET,IOCON_MODE_PULLUP,IOCON_FUNC0);
	Chip_GPIO_SetPinDIROutput(LPC_GPIO,TFT_RESET);
	Chip_IOCON_DisableOD(LPC_IOCON,TFT_RESET);
	GPIO_SetPin_HIGH(LPC_GPIO,TFT_RESET);

	Chip_IOCON_PinMux(LPC_IOCON,TFT_WR,IOCON_MODE_PULLUP,IOCON_FUNC0);
	Chip_GPIO_SetPinDIROutput(LPC_GPIO,TFT_WR);
	Chip_IOCON_DisableOD(LPC_IOCON,TFT_WR);
	GPIO_SetPin_HIGH(LPC_GPIO,TFT_WR);

	Chip_IOCON_PinMux(LPC_IOCON,TFT_RD,IOCON_MODE_PULLUP,IOCON_FUNC0);
	Chip_GPIO_SetPinDIROutput(LPC_GPIO,TFT_RD);
	Chip_IOCON_DisableOD(LPC_IOCON,TFT_RD);
	GPIO_SetPin_HIGH(LPC_GPIO,TFT_RD);

	msDelay(500);

	GPIO_SetPin_HIGH(LPC_GPIO,TFT_RESET);
	msDelay(200);
	GPIO_SetPin_LOW(LPC_GPIO,TFT_RESET);
	msDelay(200);
	GPIO_SetPin_HIGH(LPC_GPIO,TFT_RESET);
	msDelay(200);

	TFT_Write_Com_Data(0x0000,0x0001);    msDelay(1);
    TFT_Write_Com_Data(0x0003,0xA8A4);    msDelay(1);
    TFT_Write_Com_Data(0x000C,0x0000);    msDelay(1);
    TFT_Write_Com_Data(0x000D,0x080C);    msDelay(1);
    TFT_Write_Com_Data(0x000E,0x2B00);    msDelay(1);
    TFT_Write_Com_Data(0x001E,0x00B7);    msDelay(1);
    TFT_Write_Com_Data(0x0001,0x2B3F);    msDelay(1);
    TFT_Write_Com_Data(0x0002,0x0600);    msDelay(1);
    TFT_Write_Com_Data(0x0010,0x0000);    msDelay(1);
    TFT_Write_Com_Data(0x0011,0x6010);    msDelay(1);
    /*
     * Cambiado el sentido en el que se recorre la pantalla.
     * Para que quede como antes en el comando 0x0011, iba el dato 0x6070
     */
    TFT_Write_Com_Data(0x0005,0x0000);    msDelay(1);
    TFT_Write_Com_Data(0x0006,0x0000);    msDelay(1);
    TFT_Write_Com_Data(0x0016,0xEF1C);    msDelay(1);
    TFT_Write_Com_Data(0x0017,0x0003);    msDelay(1);
    TFT_Write_Com_Data(0x0007,0x0233);    msDelay(1);
    TFT_Write_Com_Data(0x000B,0x0000);    msDelay(1);
    TFT_Write_Com_Data(0x000F,0x0000);    msDelay(1);
    TFT_Write_Com_Data(0x0041,0x0000);    msDelay(1);
    TFT_Write_Com_Data(0x0042,0x0000);    msDelay(1);
    TFT_Write_Com_Data(0x0048,0x0000);    msDelay(1);
    TFT_Write_Com_Data(0x0049,0x013F);    msDelay(1);
    TFT_Write_Com_Data(0x004A,0x0000);    msDelay(1);
    TFT_Write_Com_Data(0x004B,0x0000);    msDelay(1);
    TFT_Write_Com_Data(0x0044,0xEF00);    msDelay(1);
    TFT_Write_Com_Data(0x0045,0x0000);    msDelay(1);
    TFT_Write_Com_Data(0x0046,0x013F);    msDelay(1);
    TFT_Write_Com_Data(0x0030,0x0707);    msDelay(1);
    TFT_Write_Com_Data(0x0031,0x0204);    msDelay(1);
    TFT_Write_Com_Data(0x0032,0x0204);    msDelay(1);
    TFT_Write_Com_Data(0x0033,0x0502);    msDelay(1);
    TFT_Write_Com_Data(0x0034,0x0507);    msDelay(1);
    TFT_Write_Com_Data(0x0035,0x0204);    msDelay(1);
    TFT_Write_Com_Data(0x0036,0x0204);    msDelay(1);
    TFT_Write_Com_Data(0x0037,0x0502);    msDelay(1);
    TFT_Write_Com_Data(0x003A,0x0302);    msDelay(1);
    TFT_Write_Com_Data(0x003B,0x0302);    msDelay(1);
    TFT_Write_Com_Data(0x0023,0x0000);    msDelay(1);
    TFT_Write_Com_Data(0x0024,0x0000);    msDelay(1);
    TFT_Write_Com_Data(0x0025,0x8000);    msDelay(1);
    TFT_Write_Com_Data(0x004f,0x0000);    msDelay(1);
    TFT_Write_Com_Data(0x004e,0x0000);    msDelay(1);
    TFT_Write_Com(0x0022);			    msDelay(1);

	GPIO_SetPin_HIGH(LPC_GPIO,TFT_CS);
	//msDelay(1);
	GPIO_SetPin_LOW(LPC_GPIO,TFT_CS);

	msDelay(500);
}

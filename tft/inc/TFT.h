/*
 * TFT.h
 *
 *  Created on: 27/08/2013
 *      Author: Grupo 4
 */

#ifndef TFT_H_
#define TFT_H_

////////////////DISPLAY/////////////////////

//#include	"basicFunctions.h"
#include "chip.h"

#define			TFT_DB0				0,23
#define			TFT_DB1				0,24
#define			TFT_DB2				0,25
#define			TFT_DB3				0,26
#define			TFT_DB4				0,27
#define			TFT_DB5				0,5
#define			TFT_DB6				0,6
#define			TFT_DB7				0,7
#define			TFT_DB8				0,8
#define			TFT_DB9				0,9
#define			TFT_DB10			0,10
#define			TFT_DB11			2,0
#define			TFT_DB12			2,1
#define			TFT_DB13			2,2
#define			TFT_DB14			2,3
#define			TFT_DB15			2,4
#define			TFT_RS				0,28
#define			TFT_WR				0,22
#define			TFT_RESET			2,7

#define			TFT_RD				0,21
#define			TFT_CS				2,13


#define 		TOUCH_E				0,16
#define			TOUCH_BUSY			1,30  // No se usa!
#define			TOUCH_PENIRQ		0,3
#define			TOUCH_MOSI			0,18
#define			TOUCH_MISO			0,17
#define			TOUCH_CLK			0,15

#define	SCR_WIDTH	( (int) 250 )			// Ancho de la pantalla [píxels]
#define SCR_HEIGHT	( (int) 240 )			//Altura pantalla



//copiado el estilo de chip gpio, pero no hay salto (seteas high o low directo)
STATIC INLINE void GPIO_SetPin_HIGH(LPC_GPIO_T *pGPIO, uint32_t port, uint8_t pin)
{
	pGPIO[port].SET |= 1UL << pin;

}

STATIC INLINE void GPIO_SetPin_LOW(LPC_GPIO_T *pGPIO, uint32_t port, uint8_t pin)
{
	pGPIO[port].CLR |= 1UL << pin;
}

#define			LATCH_WR			GPIO_SetPin_LOW(LPC_GPIO,TFT_WR); GPIO_SetPin_LOW(LPC_GPIO,TFT_WR);GPIO_SetPin_HIGH(LPC_GPIO,TFT_WR); GPIO_SetPin_HIGH(LPC_GPIO,TFT_WR);
#define			TFT_RS_LOW          GPIO_SetPin_LOW(LPC_GPIO,TFT_RS)
#define			TFT_RS_HIGH			GPIO_SetPin_HIGH(LPC_GPIO,TFT_RS)



extern __IO uint32_t DB[16][2];

/* Com,data,x,y,colores son 16 bits de datos
 * r,g,b son de 8 bits
 * (*stg) son 8 bits
 */
extern int write_TFT_ASM(uint32_t);
void TFT_Write (uint32_t);
STATIC INLINE void TFT_Write_Com (uint32_t Com)
{
	TFT_RS_LOW;//comando
	TFT_Write(Com);
}
STATIC INLINE void TFT_Write_Data (uint32_t Dat)
{
	TFT_RS_HIGH;//data
	TFT_Write(Dat);
}
STATIC INLINE void TFT_Write_Com_Data (uint32_t Com, uint32_t Dat)
{
	TFT_Write_Com(Com);
	TFT_Write_Data(Dat);
}

void TFT_SetData (uint32_t Sth);
void TFT_Set_Address (uint32_t x1, uint32_t y1, uint32_t x2, uint32_t y2);
uint16_t TFT_Color (uint32_t r, uint32_t g, uint32_t b);
void TFT_Screen_Color (uint32_t r, uint32_t g, uint32_t b); // bits: [7:3] r  [7:2] g [7:3] bl
void TFT_Three_Lines ( void );
//void TFT_PrintBig (uint8_t *stg, uint32_t x, uint32_t y, uint32_t color_texto, uint32_t color_fondo);
void TFT_PrintBig (uint8_t *stg, uint16_t x, uint16_t y, uint16_t color_texto, uint16_t color_fondo);
void TFT_PrintSmall (uint8_t *stg, uint16_t x, uint16_t y, uint16_t color_texto, uint16_t color_fondo);


void TFT_Black( void );
void TFT_Black_Slow( void );
void TFT_FastSendColor(uint32_t color);	//solo toma rojo y verde..
void TFT_FastErase(uint32_t color);

void InitTFT(void);

void msDelay(uint32_t delay);

#define			SMALL_X			8		//small
#define			SMALL_Y			12	//small
#define			BIG_X			16		//big
#define			BIG_Y			16		//big


extern uint8_t Small_Font[];
extern uint8_t Big_Font[];


typedef struct {
	int32_t x;
	int32_t y;
}touch_t;

void InitTouch(void);
touch_t Touch_Get ( void );




#endif /* TFT_H_ */

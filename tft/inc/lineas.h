/*
 * lineas.h
 *
 *  Created on: 06/07/2013
 *      Author: Grupo 4
 */

#ifndef		LINEAS_H
#define		LINEAS_H

#define CANTX		320
#define CANTY		240

#include	"TFT.h"

// extern	uint16_t		vector[CANTX][CANTY];

void LineaH(uint16_t x, uint16_t y, uint16_t d, uint16_t color);
/* Dibuja una linea horizontal que empieza en (x,y) y tiene
 * largo 'd' (hacia la derecha)
 */

void LineaV(uint16_t x, uint16_t y, uint16_t h, uint16_t color);
/* Dibuja una linea vertical de origen (x,y) y altura h (hacia arriba)
 */
void LineaV2(int16_t x, int16_t y1, int16_t y2, uint16_t color);

void LineaOpt(uint16_t x1, uint16_t y1, uint16_t x2, uint16_t y2, uint16_t color);
/* Dibuja una linea de coordenadas y color indiacados
 */

void set_pixel(uint16_t x, uint16_t y, uint16_t color);

#endif		/* LINEAS_H */

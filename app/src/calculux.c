/* COMO USAR:
 *
 * int calculux();
 * 	calcula Vcc y Vpp, y las guarda como strings en las globales txtVcc y txtVpp
 * 	Ajusta el tamaño de txtVcc y txtVpp según la variable global maxLenText (seteada en 5)
 * 	Lee datos del buffer global bufferOut[]
 * 	Para escalar el valor, lee la variable global EscalaV_mv (donde debe estar la tensión máxima representable en pantalla en milivolts)
 *
 * int numToText(int num, int decimales, char *str, int maxLen, char * unidad);
 * 	Recibe numero entero (num)
 * 	En "decimales" se indica cuántos de los últimos dígitos van luego del punto decimal.
 * 	En unidad recibe un char* que irá al final del texto
 * 	ej: numToText(12345, 2, buffer, 10, "volts") escribirá 123.45volts (los ultimos 2 digitos son decimales)
 */

#include "global.h"
#include "variablesGlobales.h"

int numToText(int num, int decimales, char *str, int maxLen, char * unidad);
int calculux();

int calculux(){
//	const int maxLenText=MAX_LENG_T_ITEMS;
	int i=0;
	long long acum=0;
	int max=0x0, min=0x0FFF;
	int Vcc=0, Vpp=0;
	//Vcc y Vpp
	for(i=0;i<SCREEN_WIDTH;i++){
		acum+=bufferOut[i];
		if(bufferOut[i] > max) max = bufferOut[i];
		if(bufferOut[i] < min) min = bufferOut[i];
	}
	Vcc = acum / SCREEN_WIDTH;
	Vpp = max-min;
	int32_t EscalaV_mv = currentConfig.voltDiv*10;

	if( ((Vcc-2047) * EscalaV_mv / 0x0FFF) >= 1000 || ((Vcc-2047) * EscalaV_mv / 0x0FFF) <= -1000)
		numToText((Vcc-2047)*EscalaV_mv/0x0FFF, 3, txtVcc, MAX_LENG_T_ITEMS-1, "V") ;
	else
		numToText( ((Vcc-2047)*EscalaV_mv/0x0FFF), 0, txtVcc, MAX_LENG_T_ITEMS-2, "mV") ;
	if(Vpp * EscalaV_mv / 0x0FFF >= 1000)
		numToText(Vpp*EscalaV_mv/0x0FFF, 3, txtVpp, MAX_LENG_T_ITEMS-1, "V");
	else
		numToText(Vpp*EscalaV_mv/0x0FFF, 0, txtVpp, MAX_LENG_T_ITEMS-2, "mV");

//	printf("Vcc = %s\nVpp = %s\n", txtVcc, txtVpp );

	return 0;
}

//Recibe numero entero indicando en "decimales" donde deberia estar el punto decimal
// ej: numToText(12345, 2, buffer, 10) escribira 123.45 (los ultimos 2 digitos son decimales)
//devuelve len salida. 0 si no pudo escribir.
int numToText(int num, int decimales, char *str, int maxLen, char * unidad){
	int i=0;
	char temp[100];
	int numTemp = num;
	if(numTemp < 0){
		str[0]='-';
		numTemp *= (-1);
	}else
		str[0]=' ';
	while(numTemp){
		temp[i++] = numTemp % 10;
		numTemp /= 10;
	}
	int j=1;
	if(i==decimales) str[j++]='0';
	for(--i;i+1;i--){
		if(j >= maxLen) break;
		if(i+1==decimales) str[j++] = ',';
		str[j++] = '0' + temp[i];
	}
	i=0;

	while(str[j++]=unidad[i++]); // Nada de sumar líneas. Compacto!

	return j;
}

/*
 * menu.c
 *
 *  Created on: 25/10/2015
 *      Author: nahuel
 */


#include "menu.h"
#include "FreeRTOS.h"
#include "task.h"
#include "queue.h"
#include "semphr.h"
#include "portmacro.h"
#include "variablesGlobales.h"
#include "mux.h"
#include "global.h"
#include "board.h"
#include "initApp.h"

extern xQueueHandle qTouch;
touch_t mCalib,bCalib;

int32_t validarXY(touch_t xy)
{
	int32_t x;
	int32_t y;
	x = (xy.x*mCalib.x+bCalib.x)/1000;
	y = (xy.y*mCalib.y+bCalib.y)/1000;

	if (x>159)
	{
		if (y > 175)
		{
			if (x>239)	return BUT_UP;
			else 	return BUT_DW;
		}
		else if (y<79)
		{
			if (x>239)	return BUT_IN;
			else 	return BUT_DC;
		}
	}
	else
	{
		return BUT_SAL;
	}
	return BUT_NOBUT;
}

void SetCurrentConfig()
{

	currentConfig.edgeType = trigger_type_values[iItem_value[iTriggerType]];
	currentConfig.triggerLevel = trigger_values[iItem_value[iTriggerValue]];
	currentConfig.voltDiv = esc_v_values[iItem_value[iScaleV]];
	currentConfig.timeBase_us = base_tiempo_values[iItem_value[iBT]];
	currentConfig.hist = 2;

	changeMux(7-iItem_value[iScaleV]);
	Chip_ADC_SetSampleRate(LPC_ADC, &ADCSetup, fs_values[iItem_value[iBT]] );
	//Chip_ADC_SetSampleRate(LPC_ADC, &ADCSetup, 100000/(iItem_value[3]+1));


}


void taskMenu(void) {
	int32_t button = 2;
	uint8_t i = 0;

	uint8_t salir=0;
	touch_t xy;
	//	uint8_t ** sItems;

	//	while (1) {

	//		queuereceive(qMenu, &ptr);
	//		semaphoretske(semaforoscreen);
	{
		dibujarMenu();

		/*****************************/
		//			 Test para recorrer el menu
		//					for (i=0;i<20;i++)
		//						selItem(BUT_UP);
		//					for (i=0;i<23;i++)
		//						selItem(BUT_DW);
		/*****************************/
		//
		//			/*****************************/
		////			Test para cambiar valor
		//			for (i=0;i<20;i++){
		//				chValItem(BUT_DC);
		//				printValItem();
		//			}
		//			/*****************************/

		do{
			// 1-> cola bloqueante que se queda esperando el valor de x e y
			// 2-> validación de x e y
			// 3-> actcuar en cosecuencia

			xQueueReceive(qTouch,&xy,portMAX_DELAY);
			button = validarXY(xy);

			switch (button) {
			case BUT_UP:
				selItem(BUT_UP);
				break;
			case BUT_DW:
				selItem(BUT_DW);
				break;
			case BUT_IN:
				chValItem(BUT_IN);
				break;
			case BUT_DC:
				chValItem(BUT_DC);
				break;
			case BUT_SAL:
				salir = 1;
			default:
				break;
			}
			printValItem(); // Se actualiza en valor en pantalla luego de que se presiona cualquier boton
		}while(!salir);
		//		}

		SetCurrentConfig();
		//al salir:
		//		semaforogive(semaforoscreen);
		//		queuesend(q);
	}
}

void dibujarMenu(void) {
	uint8_t j, i;
	//	uint8_t ** sItems;

	TFT_Black();
	nItem = 0;
	//Bordes divisores
	for (i = 0; i < BORDER_LINE; i++) {
		LineaV(WIDTH_FULL - W_ITEM - BORDER_LINE / 2 + i, 0, HEIGHT_FULL,
				COLOR_BORDER_LINE);
		LineaV(WIDTH_FULL - W_BUT - BORDER_LINE / 2 + i, 0, H_BUT_UP,
				COLOR_BORDER_LINE);
		LineaV(WIDTH_FULL - W_BUT - BORDER_LINE / 2 + i, H_BUT_IN + H_VAL,
				H_BUT_IN, COLOR_BORDER_LINE);
		LineaH(WIDTH_FULL - W_ITEM, H_BUT_IN - BORDER_LINE / 2 + i, W_ITEM,
				COLOR_BORDER_LINE);
		LineaH(WIDTH_FULL - W_ITEM, H_BUT_IN + H_VAL - BORDER_LINE / 2 + i,
				W_ITEM, COLOR_BORDER_LINE);
	}

	//Items y sus divisiones
	i = 0;
	TFT_PrintBig(sItems[i], MARGIN_T_LEFT, MARGIN_T_BOTT_1 + H_EQ_ITEM * i,
			COLOR_T_LETRA, COLOR_T_FONDO);
	TFT_PrintBig(sItems[i+1], MARGIN_T_LEFT, MARGIN_T_BOTT_2 + H_EQ_ITEM * (i+1),
							COLOR_T_LETRA, COLOR_T_FONDO);
	for (i = 1; i < N_ITEMS ; i++) {
		TFT_PrintBig(sItems[i*2], MARGIN_T_LEFT, MARGIN_T_BOTT_1 + H_EQ_ITEM * i*2,
				COLOR_T_LETRA, COLOR_T_FONDO);
		TFT_PrintBig(sItems[i*2+1], MARGIN_T_LEFT, MARGIN_T_BOTT_2 + H_EQ_ITEM * (i*2+1),
						COLOR_T_LETRA, COLOR_T_FONDO);
		LineaH(0, H_ITEM * i, W_ITEM, COLOR_ITEM_LINE);
	}

	//Primer item sombreado, posicion del vector de strings 15
	for (j = 0; j < H_ITEM; j++)
		LineaH(0, j, W_ITEM, COLOR_T_LETRA);

	TFT_PrintBig(sItems[0], MARGIN_T_LEFT, MARGIN_T_BOTT_1 + H_EQ_ITEM * (0),
			COLOR_T_FONDO, COLOR_T_LETRA);
	TFT_PrintBig(sItems[1], MARGIN_T_LEFT, MARGIN_T_BOTT_2 + H_EQ_ITEM * 1,
				COLOR_T_FONDO, COLOR_T_LETRA);

	//Botones UP y DOWN
	for (i = 0; i < (S_BUT_UP / 2) + 1; i++) {
		LineaH(PX_BUT_UP + i, PY_BUT_UP + i, S_BUT_UP - 2 * i, COLOR_BUT_UP);
		LineaH(PX_BUT_DW + i, PY_BUT_DW - i, S_BUT_DW - 2 * i, COLOR_BUT_DW);
	}

	//Botones + y -
	for (i = 0; i < S2_BUT_IN; i++) {
		LineaH(PX1_BUT_IN, PY1_BUT_IN - i, S1_BUT_IN, COLOR_BUT_IN);
		LineaV(PX2_BUT_IN + i, PY2_BUT_IN, S1_BUT_IN, COLOR_BUT_IN);
		LineaH(PX_BUT_DC, PY_BUT_DC - i, S1_BUT_DC, COLOR_BUT_DC);
	}

	//Valor del primer item
	//El que queda por default sombreado, ie. seleccionado
//	TFT_PrintSmall(pItem_string[nItem][ iItem_value[nItem] ] , MARGIN_X1_T_ITEM_VALUE, MARGIN_Y_T_ITEM_VALUE, COLOR_T_LETRA_VALUE, COLOR_T_FONDO);
	printValItem();
}

void selItem(uint8_t d) {
	/*Se saca el sombreado al item actual y se le agrega al próximo
	 */
	uint8_t i, new_nItem = 0;

	if (d != BUT_UP && d != BUT_DW)
		return;

	if (d == BUT_UP)
		//Hacia arriba
		new_nItem = (nItem + 1) % N_ITEMS;
	else {
		//Hacia abajo
		if (nItem)
			new_nItem = nItem - 1;
		else
			new_nItem = N_ITEMS - 1; //Pega la vuelta
	}

	for (i = 0; i < H_ITEM - ITEM_LINE; i++) {
		LineaH(0, H_ITEM * nItem + ITEM_LINE + i , W_ITEM-BORDER_LINE/2, COLOR_T_FONDO); // Item actual
		LineaH(0, H_ITEM * new_nItem + ITEM_LINE + i , W_ITEM-BORDER_LINE/2, COLOR_T_LETRA); // Item futuro, contiguo al actual
	}
	TFT_PrintBig(sItems[2*nItem], MARGIN_T_LEFT,
			MARGIN_T_BOTT_1 + H_EQ_ITEM * (2*nItem), COLOR_T_LETRA, COLOR_T_FONDO); //Item actual sin sombra
	TFT_PrintBig(sItems[2*nItem+1], MARGIN_T_LEFT,
			MARGIN_T_BOTT_2 + H_EQ_ITEM * (2*nItem+1), COLOR_T_LETRA, COLOR_T_FONDO); //Item actual sin sombra

	TFT_PrintBig(sItems[2*new_nItem], MARGIN_T_LEFT,
			MARGIN_T_BOTT_1 + H_EQ_ITEM * (2*new_nItem), COLOR_T_FONDO, COLOR_T_LETRA); //Item futuro sombreado
	TFT_PrintBig(sItems[2*new_nItem+1], MARGIN_T_LEFT,
			MARGIN_T_BOTT_2 + H_EQ_ITEM * (new_nItem*2+1), COLOR_T_FONDO, COLOR_T_LETRA); //Item futuro sombreado


	nItem = new_nItem;

	return;
}


//TODO ESTO NO ME GUSTA!!! Muy poco practico.
void printValItem(void) {
	// Imprime en el sector entre medio de los botones
	// el valor del item indicado por el indice nItem

	// Se borra el valor anterior
	TFT_PrintBig	("AAAAAAAAAA", WIDTH_FULL/2 + BORDER_LINE , MARGIN_Y_T_ITEM_VALUE,COLOR_T_FONDO, COLOR_T_FONDO);

	if (pItem_string[nItem]==base_tiempo_string){
		/*if (iItem_value[nItem] == (N_BASE_TIEMPO-1)){
			// Segundo/Div
			TFT_PrintBig	(pItem_string[nItem][ iItem_value[nItem] ] , MARGIN_X1_T_ITEM_VALUE, MARGIN_Y_T_ITEM_VALUE,COLOR_T_LETRA_VALUE, COLOR_T_FONDO);
			TFT_PrintSmall	(unidades_BT_string[2] , MARGIN_X2_T_ITEM_VALUE, MARGIN_Y_T_ITEM_VALUE,COLOR_T_LETRA_VALUE, COLOR_T_FONDO);
		}*/
		if (iItem_value[nItem] < UNIDAD_BASE_TIEMPO){
			// uS/Div
			TFT_PrintBig	(pItem_string[nItem][ iItem_value[nItem] ] , MARGIN_X1_T_ITEM_VALUE, MARGIN_Y_T_ITEM_VALUE,COLOR_T_LETRA_VALUE, COLOR_T_FONDO);
			TFT_PrintSmall	(unidades_BT_string[0] , MARGIN_X2_T_ITEM_VALUE, MARGIN_Y_T_ITEM_VALUE,COLOR_T_LETRA_VALUE, COLOR_T_FONDO);
		}
		else{
			// mS/Div
			TFT_PrintBig	(pItem_string[nItem][ iItem_value[nItem] ] , MARGIN_X1_T_ITEM_VALUE, MARGIN_Y_T_ITEM_VALUE,COLOR_T_LETRA_VALUE, COLOR_T_FONDO);
			TFT_PrintSmall	(unidades_BT_string[1] , MARGIN_X2_T_ITEM_VALUE, MARGIN_Y_T_ITEM_VALUE,COLOR_T_LETRA_VALUE, COLOR_T_FONDO);
		}
	}
	else if (pItem_string[nItem]==esc_v_string){
		if (iItem_value[nItem]<UNIDAD_ESC_V){
//		if (nItem< UNIDAD_ESC_V){
			TFT_PrintBig	(pItem_string[nItem][ iItem_value[nItem] ] , MARGIN_X1_ESC_V, MARGIN_Y_T_ITEM_VALUE,COLOR_T_LETRA_VALUE, COLOR_T_FONDO);
			TFT_PrintSmall	(undiades_V_string[1] , MARGIN_X2_ESC_V, MARGIN_Y_T_ITEM_VALUE,COLOR_T_LETRA_VALUE, COLOR_T_FONDO);
		}
		else{
			TFT_PrintBig	(pItem_string[nItem][ iItem_value[nItem] ] , MARGIN_X1_T_ITEM_VALUE, MARGIN_Y_T_ITEM_VALUE,COLOR_T_LETRA_VALUE, COLOR_T_FONDO);
			TFT_PrintSmall	(undiades_V_string[0] , MARGIN_X2_ESC_V, MARGIN_Y_T_ITEM_VALUE,COLOR_T_LETRA_VALUE, COLOR_T_FONDO);
		}
	}
	else if (pItem_string[nItem]==trigger_type_string)
		TFT_PrintBig	(pItem_string[nItem][ iItem_value[nItem] ] , MARGIN_X3_T_ITEM_VALUE, MARGIN_Y_T_ITEM_VALUE,COLOR_T_LETRA_VALUE, COLOR_T_FONDO);
	else
		TFT_PrintBig	(pItem_string[nItem][ iItem_value[nItem] ] , MARGIN_X4_T_ITEM_VALUE, MARGIN_Y_T_ITEM_VALUE,COLOR_T_LETRA_VALUE, COLOR_T_FONDO);
	return;
}

uint32_t chValItem(uint32_t a) {
	// change Value Item
	// cambia el valor del item idicado por nItem

	if (a != BUT_IN && a != BUT_DC)
		return 0;

	// Si se encuentra sobre el Item más bajo en la patalla, el 'return'
	// al apretar cualquier boton BUT_IC o BUT_DC vuelve a la pantalla principal
//	if (nItem == 0)
//		return 1;

	if (a == BUT_IN)
		iItem_value[nItem]=(iItem_value[nItem]+1)%Item_valueMAX[nItem];
	else
		// hace lo mismo que lo de abajo
		iItem_value[nItem]=(iItem_value[nItem]+Item_valueMAX[nItem] -1)%Item_valueMAX[nItem];

	/*if (!vItem[nItem]) // valor minimo
			vItem[nItem] = vItem_valueMAX[nItem]; // pega la vuelta
		else
			vItem[nItem]--;*/

	return 0;
}


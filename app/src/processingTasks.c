//
#include "../inc/processingTasks.h"
//

#include "FreeRTOS.h"
#include "../inc/globalRTOS.h"
#include "global.h"
#include "lineas.h"
#include "taskSenial.h"
#include <string.h>
#include "mux.h"
#include "initApp.h"

void ADC_IRQHandler(void)
{
	uint16_t val=0;
	static portBASE_TYPE xHigherPriorityTaskWoken = pdFALSE;

	Chip_ADC_ReadValue(LPC_ADC,ADC_CH7,&val);
	//	if (Chip_GPDMA_Interrupt(LPC_GPDMA, dmaChannel) == SUCCESS)
	//	{
	//		xQueueOverwriteFromISR(qDMA, &cursor, &xHigherPriorityTaskWoken);
	//		cursor = (cursor + SIZE_INTERRUPT_DMA) % SIZE_DMA;
	//		Chip_GPDMA_Transfer(LPC_GPDMA, dmaChannel, GPDMA_CONN_ADC, (unsigned int)bufferDMA+cursor, GPDMA_TRANSFERTYPE_P2M_CONTROLLER_DMA, SIZE_INTERRUPT_DMA);
	//		portYIELD_FROM_ISR( xHigherPriorityTaskWoken );
	//	}

}
/*
 * Se modifico la libreria del DMA para tocar el channel 7.
 *
 *
 */
void DMA_IRQHandler (void)
{
	static int cursor =0;
	static portBASE_TYPE xHigherPriorityTaskWoken = pdFALSE;
	if (Chip_GPDMA_Interrupt(LPC_GPDMA, dmaChannel) == SUCCESS)
	{
		xQueueOverwriteFromISR(qDMA, &cursor, &xHigherPriorityTaskWoken);
		cursor = (cursor + SIZE_INTERRUPT_DMA) % SIZE_DMA;
		Chip_GPDMA_Transfer(LPC_GPDMA, dmaChannel, GPDMA_CONN_ADC, (uint32_t)(bufferDMA+cursor), GPDMA_TRANSFERTYPE_P2M_CONTROLLER_DMA, SIZE_INTERRUPT_DMA);
		portYIELD_FROM_ISR( xHigherPriorityTaskWoken );
	}
}


int32_t trigger (int32_t v)
{
	if (currentConfig.edgeType == edgeRISE)
	{
		if (v >(currentConfig.triggerLevel+ currentConfig.hist)) return 1;
	}
	else
	{
		if (v < (currentConfig.triggerLevel- currentConfig.hist)) return 1;
	}
	return 0;
}


int interpolar(int buffIdx,int ups)
{
	uint32_t i,j,offset,aux;
	int32_t a0,a1,a2,a3,m;
	int32_t xp2,xp1,x,xm1; // Plus or minux #

	xp2 = ADC_DR_RESULT(bufferDMA[(2+buffIdx)%SIZE_DMA]);
	xp1 = ADC_DR_RESULT(bufferDMA[(1+buffIdx)%SIZE_DMA]);
	x = ADC_DR_RESULT(bufferDMA[buffIdx]);
	xm1= ADC_DR_RESULT(bufferDMA[(SIZE_DMA+buffIdx-1)%SIZE_DMA]);
	a0 =  xp2 - xp1 - xm1 + x;
	a1 = xm1 - x - a0;
	a2 = xp1 - xm1;
	a3 = x;

	j=0;
	offset=0;

	do // Trigger
	{
		m=(j*100)/ups;

		bufferOut[0] = ((a0/100)*m*m*m+a1*m*m+(a2*100)*m+(100*100)*a3)/(100*100);
		j++;
	}while(!trigger(bufferOut[0]) && j<ups);

	aux = j;
	if (j!=ups)
	{
		offset = ups-j;
		for(j=aux;j<ups;j++)
		{
			m=((j+aux)*100)/ups;
			bufferOut[j-aux] = ((a0/100)*m*m*m+a1*m*m+(a2*100)*m+(100*100)*a3)/(100*100);
		}
		buffIdx++;
	}
	else offset = 0;



	for (i=0;i < (SIZE_OUT-offset)/ups;i++,buffIdx++) // Lo del medio
	{
		xp2 = ADC_DR_RESULT(bufferDMA[(2+buffIdx)%SIZE_DMA]);
		xp1 = ADC_DR_RESULT(bufferDMA[(1+buffIdx)%SIZE_DMA]);
		x = ADC_DR_RESULT(bufferDMA[buffIdx]);
		xm1= ADC_DR_RESULT(bufferDMA[(SIZE_DMA+buffIdx-1)%SIZE_DMA]);

		a0 =  xp2 - xp1 - xm1 + x;
		a1 = xm1 - x - a0;
		a2 = xp1 - xm1;
		a3 = x;

		for(j=0;j<ups;j++)
		{
			m=(j*100)/ups;
			bufferOut[i*ups+j+offset] = ((a0/100)*m*m*m+a1*m*m+(a2*100)*m+(100*100)*a3)/(100*100);

		}
	}
	if ((SIZE_OUT-offset)%ups) // Si falta al final
	{
		xp2 = ADC_DR_RESULT(bufferDMA[(2+buffIdx)%SIZE_DMA]);
		xp1 = ADC_DR_RESULT(bufferDMA[(1+buffIdx)%SIZE_DMA]);
		x = ADC_DR_RESULT(bufferDMA[buffIdx]);
		xm1= ADC_DR_RESULT(bufferDMA[(SIZE_DMA+buffIdx-1)%SIZE_DMA]);

		a0 =  xp2 - xp1 - xm1 + x;
		a1 = xm1 - x - a0;
		a2 = xp1 - xm1;
		a3 = x;

		for(j=0;j<(SIZE_OUT-offset)%ups;j++)
		{
			m=(j*100)/ups;
			bufferOut[i*ups+j+offset] = ((a0/100)*m*m*m+a1*m*m+(a2*100)*m+(100*100)*a3)/(100*100);

		}
	}

	return 0;

}



int32_t searchTrigger ( int32_t cursor, int32_t * trigger)
{
	static int validacion=0;
	static int state=-1;
	int CANT_VALID=3;

	if (currentConfig.timeBase_us<2500) CANT_VALID=1;

	int n=0;
	int * newBuf = bufferDMA + cursor;
	do{
		if(state==lastLOW){
			if(ADC_DR_RESULT(newBuf[n]) > currentConfig.triggerLevel + currentConfig.hist){
				validacion++;
			}else{
				validacion=0;
			}
			if(validacion == CANT_VALID){
				state = lastHIGH;
				validacion=0;
				if(currentConfig.edgeType == edgeRISE){
					*trigger = (cursor + n + SIZE_DMA - CANT_VALID)%SIZE_DMA;
					return 1;
				}
			}
		}else if (state==lastHIGH){
			if(ADC_DR_RESULT(newBuf[n])< currentConfig.triggerLevel - currentConfig.hist){
				validacion++;
			}else{
				validacion=0;
			}
			if(validacion == CANT_VALID){
				state = lastLOW;
				validacion=0;
				if(currentConfig.edgeType == edgeFALL){
					*trigger = (cursor + n + SIZE_DMA - CANT_VALID)%SIZE_DMA;
					return 1;
				}
			}
		}else{ //dont know
			if(ADC_DR_RESULT(newBuf[n]) >= currentConfig.triggerLevel + currentConfig.hist) state=lastHIGH;
			if(ADC_DR_RESULT(newBuf[n]) < currentConfig.triggerLevel - currentConfig.hist) state=lastLOW;
		}
	}while(++n < SIZE_INTERRUPT_DMA);

	return 0;
}

void ADC_MaskValuesDMABuff ()
{
	int i;
	for (i=0;i<SIZE_DMA;i++)
	{
		bufferDMA[i] = ADC_DR_RESULT(((uint32_t*)bufferDMA)[i]);
	}
}





void procTask (void *p)
{
	int32_t idx;
	int32_t idxTrigger;
	int32_t i,aux;
	InitMux();
	initAPP();

	while(1){

		//Prender DMA
		// Esperar la queue del dma y luego apagar dma (en la irq)

		dmaChannel = Chip_GPDMA_GetFreeChannel(LPC_GPDMA, GPDMA_CONN_ADC);
		Chip_GPDMA_Transfer(LPC_GPDMA, dmaChannel, GPDMA_CONN_ADC, (uint32_t)bufferDMA, GPDMA_TRANSFERTYPE_P2M_CONTROLLER_DMA, SIZE_INTERRUPT_DMA);

		xQueueReceive(qDMA,&idx,portMAX_DELAY);  // Una antes
		do{
			xQueueReceive(qDMA,&idx,portMAX_DELAY);
		}while(!searchTrigger(idx,&idxTrigger));
		xQueueReceive(qDMA,&idx,portMAX_DELAY); // Una despues

		Chip_GPDMA_Stop(LPC_GPDMA,dmaChannel);

		xSemaphoreTake( semBufferOut,portMAX_DELAY);

		if (currentConfig.timeBase_us >= 2500 ) //
		{
			for (i=0;i<SIZE_OUT;i++)
			{
				bufferOut[i] = ADC_DR_RESULT(bufferDMA[(idxTrigger+i)%SIZE_DMA]);
			}


		}
		else{ 		//BT = 50u,100u,250u,500u,1000u
			interpolar2(idxTrigger,2500/currentConfig.timeBase_us);
		}



		for (i=1;i<SIZE_OUT-2;i++)
		{
			aux = bufferOut[i]-bufferOut[i-1];
			if ( aux >1000 || aux < -1000)
				bufferOut[i]=(bufferOut[i+2]+bufferOut[i-2])/2;
		}

		xSemaphoreGive( semBufferOut);
		xSemaphoreGive( semNewImage);
	}
}





/*
 *	@brief Searchs for trigger condition.
 *	When finds edge, pushes in a queue the position of the edge (pointer to element) and disables the trigger.
 * 	To trigger again, a task has to send an 'Enable' command using queue qTriggerEnable.

void tSearchTrigger( void *pvParameters ){
	static int state;
	//static int i=0,j=0,k=0;
	int n=0;
	static int *newBuf;
	static int cursor;
	static int edgeType;
	static int found=0;


	while(1){

		// Espera habilitación del trigger
		xQueueReceive(qTriggerEnable, &edgeType, portMAX_DELAY);
		state = lastDK; //dont know
		found = 0;

		while(!found){

			// Espera dato del dma.
			xQueueReceive(qCursor, &cursor, portMAX_DELAY);
			// newBuf apunta a último buffer cargado
			newBuf = bufferDMA+cursor;
			n=0;

			do{
				// Busca flanco
				if(state==lastLOW){
					if(newBuf[n] > currentConfig.triggerLevel + currentConfig.hist){ // DECLARAR!!!
						state = lastHIGH;
						if(edgeType==edgeRISE){
							int dummy = cursor+n; //Posición del flanco //int dummy = (int) (newBuf+n-bufferDMA);
							xQueueSend(qEdgeCursor, &dummy, portMAX_DELAY); //push(qEdgePosition, newBuf+n);
							found = 1;
							break;
						}
					}
				}else if (state==lastHIGH){
					if(newBuf[n] < currentConfig.triggerLevel - currentConfig.hist){
						state = lastLOW;
						if(edgeType==edgeFALL){
							int dummy = cursor+n; //Posición del flanco
							xQueueSend(qEdgeCursor, &dummy, portMAX_DELAY); //push(qEdgePosition, newBuf+n);
							found = 1;
							break;
						}
					}
				}else{ //dont know
					if(newBuf[n] >= currentConfig.triggerLevel + currentConfig.hist) state=lastHIGH;
					if(newBuf[n] < currentConfig.triggerLevel - currentConfig.hist) state=lastLOW;
				}
			}while(n++<SIZE_INTERRUPT_DMA && !found);
		}

	}
}



 *	@brief Acts when the triggers. Copies data to process into qBufferProc.
 *	Gets position of trigger, calculates from where starts the data to be processed.

void tCopyBuffer( void *pvParameters ){//prioridad alta

	static int currentCursor;
	static int edgeCursor;
	static int startCursor;
	static int cantTotal;	//agregado pauno
	static int lastLoadedValue;
	static int i;
	static int decCount;
	static int acum = 0;

	while(1){

		// Trigger. Espera flanco. En qEdgeCursor se carga el cursor que indica en qué posición fue el flanco
		xQueueReceive(qEdgeCursor, &edgeCursor, portMAX_DELAY);
		//edgeCursor = distancia a inicio

		// Cuando llega, obtiene el cursor del dma (para saber por donde está escribiendo actualmente)
		currentCursor = edgeCursor/SIZE_INTERRUPT_DMA; // división entera para saber cuál es la posición del último buffer cargado

		//startCursor= (SIZE_DMA + edgeCursor - samplesPerScreen - samplesBeforeEdge) % SIZE_DMA;
		//int samplesPerScreen = SPS * currentConfig.timeBase_us/1000000
		int samplesPerScreen = currentConfig.timeBase_us /5; //SPS=200000 --> 200000/1000000=5
		startCursor= (SIZE_DMA + edgeCursor - samplesPerScreen - cantidades.sizeInputBeforeEdge) % SIZE_DMA;

		//TODO: CHEQUEALO KUKU --> JOYA!!
		//finish = sum(startCursor,cantTotal,SIZE_DMA);
		//cantTotal = 3*samplesPerScreen+2*samplesBeforeEdge;
		cantTotal = cantidades.totalInputSize;
		lastLoadedValue = currentCursor + SIZE_INTERRUPT_DMA - 1;
		i=0;

		decCount = cantidades.downsamplingFactor;
		acum = 0;

		while(i<cantTotal){
			//TODO: CHEQUEALO KUKU (Mirar el > y no ==)  --> PIPÍ CUCÚ!! (la cabeza me hizo chequendengue)
			if((startCursor+i)%SIZE_DMA > lastLoadedValue){
				// El dato todavía no está. Espera a que el dma cargue el buffer
				xQueueReceive( qCursor, &currentCursor, portMAX_DELAY);
				lastLoadedValue = currentCursor + SIZE_INTERRUPT_DMA-1;
			}
			acum += bufferDMA[(startCursor+i)%SIZE_DMA];
			decCount--;
			if(!decCount){
				bufferTemp[i] = acum / cantidades.downsamplingFactor;
				acum = 0;
				decCount = cantidades.downsamplingFactor;
			}
			i++;
		}

		int dummy=1;
		xQueueOverwrite(qStartProcessing, &dummy); //envía cualquier cosa, sólo señalización

	}

}

 * @brief Process the last aquisition and converts to a printable signal (interpolating if needed)
 *	The result of the process is stored in the buffer pointed by ptrBufferProcessing.
 *	When finishes, exchanges pointers ptrBufferProcessing and ptrFullBuffer.
 *

void tProcessData( void *pvParameters ){
	static int i=0,dummy=0;
	static int * temp;

	while(1){

		/// Espera habilitación para empezar a generar buffer
		xQueueReceive(qStartProcessing, &dummy, portMAX_DELAY);

		if(cantidades.upsamplingFactor > 1){
			//interpolar
		}else{
			//copiar directamente
			for(i=0;i<3*SCREEN_WIDTH;i++){
				ptrBufferProcessing[i] = bufferTemp[i];
			}

		}
		// Terminó de copiar en bufferProcessing

		xSemaphoreTake(semBufferData, portMAX_DELAY);
		{
			// Intercambia punteros ptrBufferProcessing y ptrFullBuffer
			temp								= ptrFullBuffer;
			ptrFullBuffer				= ptrBufferProcessing;
			ptrBufferProcessing = temp;
		}
		xSemaphoreGive(semBufferData);

		// Envía señal de refresh para la pantalla
		int * ptrStartScreen = ptrFullBuffer + SCREEN_WIDTH + currentConfig.timeOffset; //a partir de la 2da pantalla, + time delay
		xQueueOverwrite(qDrawSignal, &ptrStartScreen);

	}

}

 *	@brief Draws Signal on screen
 * 	Waits for sync with queue qDrawSignal.
 * 	Takes semaphore semScreen (mutex) when starts drawing and
 * 	gives it away when finishes.

void tDrawSignal( void *pvParameters )
{
	static int * ptrScreen;

	while(1){

		/// Espera señal para dibujar
		xQueueReceive(qDrawSignal, &ptrScreen, portMAX_DELAY);

		/// Toma el semáforo semBufferData para ser no se escriba el buffer mientras grafica.
		xSemaphoreTake(semBufferData, portMAX_DELAY);
		{
			/// Toma el semáforo semScreen para acceder a la pantalla
			xSemaphoreTake(semScreen, portMAX_DELAY);
			{
				 Acá función de refrescar pantalla usando ptrFullData
				//taskSenial(); //No es una task, ehh (y hay que hacerla bien...)
				drawSignal(ptrScreen, SCREEN_WIDTH, COLOR_SIGNAL, 1);
			}
			xSemaphoreGive(semScreen);
		}
		xSemaphoreGive(semBufferData);

		/// Si está en modo continuo, habilita nuevamente el Trigger
		if(currentConfig.mode == modeCONTINUOUS)
			xQueueOverwrite(qTriggerEnable, &(currentConfig.edgeType));
	}

}
 */


void actualizarCantidades(){
	/*
	const int32_t sizeOut = 3*SCREEN_WIDTH;
	const int32_t fs_out_max_MHz = SCREEN_WIDTH/LOWER_BT;
	int32_t fs_out_KHz		= 1000*SCREEN_WIDTH/BT_us; // 1000/0.2us=5000KHz
	cantidades.sincEffectiveSize			= getSincSize(currentConfig.timeBase_us);
	int32_t sizeIn		= (sizeOut+2*sizeSinc)*fs_in/fs_out; // (3*BT_us * SPS +  2 * sizeSinc * SPS / fs_out_KHz / 1000) / 1000000  ;
	upsamplingFactor	= SCREEN_WIDTH * 1000000 / currentConfig.timeBase_us / SPS; //currentConfig.upsamplingFactor	= 250 * 1000000 / currentConfig.timeBase_us / 200000;
	downsamplingFactor		= fs_in / fs_out;
	 */
	cantidades.sincEffectiveSize			= MAXSINCSIZE * LOWER_BT / currentConfig.timeBase_us;
	cantidades.upsamplingFactor				= 1250 / currentConfig.timeBase_us;	//cambiar por expresión genérica si cambian los defines.
	cantidades.downsamplingFactor			= currentConfig.timeBase_us / 1250;	//cambiar por expresión genérica si cambian los defines.
	cantidades.sincStep								= currentConfig.timeBase_us / LOWER_BT;

	cantidades.sizeInputBeforeEdge		= (cantidades.sincEffectiveSize + SCREEN_WIDTH) * currentConfig.timeBase_us / 1250 ; // (sincEffectiveSize+SCREEN_WIDTH)/upsamplingFactor
	cantidades.totalInputSize					= 3 * currentConfig.timeBase_us / 5 + cantidades.sincEffectiveSize * currentConfig.timeBase_us / 625;	// Simplificadas varias cuentas. RECALCULAR SI SE CAMBIAN DEFINES
	// cantidades.totalInputSize			= 3 * (currentConfig.timeBase_us * SPS / 1000000) + 2 * cantidades.sincEffectiveSize / cantidades.upsamplifgFactor

	if(cantidades.upsamplingFactor < 1) cantidades.upsamplingFactor = 1;
	if(cantidades.downsamplingFactor < 1) cantidades.downsamplingFactor = 1;




}


#define SCALED(value) (SCREEN_HEIGHT/2 + (value-ZERO_ADC) * SCREEN_HEIGHT / FULLSCALE_ADC )-1
void drawSignal(int *buffer, uint16_t color, int eraseOld){
	static int y1_old[SCREEN_WIDTH], y2_old[SCREEN_WIDTH];
	int i = 0;

	dibujarGrilla();
	for(i=0;i<SCREEN_WIDTH-1;i++){
		LineaV2(i, y1_old[i], y2_old[i], COLOR_FONDO);
		y1_old[i] = SCALED(buffer[i]);
		y2_old[i] = SCALED(buffer[i+1]);
		LineaV2(i, y1_old[i], y2_old[i], color);
	}
	return;
}

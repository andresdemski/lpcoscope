
#include "../inc/globalRTOS.h"
//includes for semaphores and queues in globalRTOS.h

/// Queue de tamaño 1 (se pisan valores) que el dma actualiza con el cursor a donde apunta el último buffer cargado.
xQueueHandle qDMA;
/// Queue de tamaño 1. La lee searchTrigger().
xQueueHandle qTriggerEnable;
/// Queue con posición del flanco encontrado (apunta a dirección de memoria dónde ocurrió el flanco)
xQueueHandle qEdgeCursor;
/// Queue que indica que se empiece a interpolar
xQueueHandle qStartProcessing;
/// Queue que indica que se refresque la pantalla
xQueueHandle qDrawSignal;
/// Queue que envía el comando que se seleccionó
xQueueHandle qTouchCmd;
//static xQueueHandle qQUEUE_NAME= NULL;

xSemaphoreHandle semNewImage;
xSemaphoreHandle semBufferOut;


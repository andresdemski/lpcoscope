/* Copyright 2015, Pablo Ridolfi
 *
 * This file is part of TD2-Template.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

/** @brief This is a simple C example file.
 **
 **/

/** \addtogroup TD2 Técnicas Digitales II
 ** @{ */

/** @addtogroup App Aplicación de usuario
 * 	@{
 */

/*
 * Initials     Name
 * ---------------------------
 * PR           Pablo Ridolfi
 */

/*
 * modification history (new versions first)
 * -----------------------------------------------------------
 * 20150421 v0.0.1   PR first version
 */

/*==================[inclusions]=============================================*/


#include "main.h"
/*==================[macros and definitions]=================================*/

/*==================[internal data declaration]==============================*/

xSemaphoreHandle semTouch;
xQueueHandle  qTouch;

/*==================[internal functions declaration]=========================*/

/** @brief hardware initialization function
 *	@return none
 */
static void initHardware(void);

/*==================[internal data definition]===============================*/




/*==================[external data definition]===============================*/

/*==================[internal functions definition]==========================*/




// Interrupción para el touch: Cada vez que se toca la pantalla viene a esta irq
void EINT3_IRQHandler (void)
{
	const int pin[]={TOUCH_PENIRQ};
	static signed portBASE_TYPE xHigherPriorityTaskWoken;


	if ( Chip_GPIOINT_IsIntPending(LPC_GPIOINT ,pin[0]))
	{
		// Desabilito la irq para que no salte varias veces
		Chip_GPIOINT_SetIntFalling(LPC_GPIOINT, pin[0], 0);
		Chip_GPIOINT_ClearIntStatus(LPC_GPIOINT , pin[0], 1<<pin[1]);
		xSemaphoreGiveFromISR ( semTouch, &xHigherPriorityTaskWoken);
	}

	portYIELD_FROM_ISR( xHigherPriorityTaskWoken );
	return;
}


/*
  	static int cursor=0;
	static portBASE_TYPE xHigherPriorityTaskWoken = pdFALSE;

	if (Chip_GPDMA_Interrupt(LPC_GPDMA, dmaChannelNum) == SUCCESS){
//		xQueueOverwriteFromISR(qCursor, &cursor, &xSwitchRequired);
		cursor = (cursor + SIZE_INTERRUPT_DMA) % SIZE_DMA;
		Chip_GPDMA_Transfer(LPC_GPDMA, dmaChannelNum, GPDMA_CONN_ADC, (unsigned int)bufferDMA+cursor, GPDMA_TRANSFERTYPE_P2M_CONTROLLER_DMA, SIZE_INTERRUPT_DMA);
	//	portYIELD_FROM_ISR( xHigherPriorityTaskWoken );

	return;
 */


static void initHardware(void)
{
	SystemCoreClockUpdate();
	Board_Init();
	//Board_LED_Set(0, false);
	//SysTick_Config(SystemCoreClock/1000);

	InitTFT();
	InitTouch();

	usbAttached=0;
	Chip_USB_Init();
	USB_Init(FlashDisk_MS_Interface.Config.PortNumber, USB_MODE_Host);

	Chip_Clock_EnablePeriphClock(SYSCTL_CLOCK_GPIO);
	NVIC_EnableIRQ(EINT3_IRQn);
}

/*
static void touchTask(void * p)
{
	const int pin[]={TOUCH_PENIRQ};
	touch_t xy,xy_r;
	const int PROM = 20;
	uint32_t i =0;
	xSemaphoreTake( semTouch,0);

	while (1)
	{
		//Habilito IRQ
		Chip_GPIOINT_ClearIntStatus(LPC_GPIOINT , pin[0], 1<<pin[1]);
		Chip_GPIOINT_SetIntFalling(LPC_GPIOINT, pin[0], 1<<pin[1]);

		// Me quedo esperando a que llegue la irq
		if (xSemaphoreTake( semTouch,portMAX_DELAY) )
		{
			xy_r.x = 0;
			xy_r.y = 0;
			vTaskDelay(100);
			for (i=0;i<PROM;i++)
			{
				vTaskDelay(10);
				xy = Touch_Get();
				xy_r.x += xy.x;
				xy_r.y += xy.y;
			}
			xy_r.x = xy_r.x/PROM;
			xy_r.y = xy_r.y/PROM;

			xQueueSend(qTouch,&xy_r,portMAX_DELAY);
			vTaskDelay(400);
		}
	}
}*/

static void touchTask(void * p){
	const int pin[]={TOUCH_PENIRQ};
	touch_t xy,xy_r;
	const int CANT = 11;
	int32_t X[CANT],Y[CANT];
	int32_t c,d,swap;
	xSemaphoreTake( semTouch,0);

	while (1){
		//Habilito IRQ
		Chip_GPIOINT_ClearIntStatus(LPC_GPIOINT , pin[0], 1<<pin[1]);
		Chip_GPIOINT_SetIntFalling(LPC_GPIOINT, pin[0], 1<<pin[1]);
		// Me quedo esperando a que llegue la irq
		if (xSemaphoreTake( semTouch,portMAX_DELAY) ){
			xy_r.x = 0;
			xy_r.y = 0;
			vTaskDelay(10);//100
			for (c=0;c<CANT;c++){
				vTaskDelay(10);
				xy = Touch_Get();
				X[c] = xy.x;
				Y[c] = xy.y;
			}
			//bubble sort for X
			for (c = 0 ; c < ( CANT - 1 ); c++){
				for (d = 0 ; d < CANT - c - 1; d++){
					if (X[d] > X[d+1]){ /* For decreasing order use < */
						swap   = X[d];
						X[d]   = X[d+1];
						X[d+1] = swap;
					}
				}
			}
			//bubble sort for Y
			for (c = 0 ; c < ( CANT - 1 ); c++){
				for (d = 0 ; d < CANT - c - 1; d++){
					if (Y[d] > Y[d+1]){ /* For decreasing order use < */
						swap   = Y[d];
						Y[d]   = Y[d+1];
						Y[d+1] = swap;
					}
				}
			}
			// el resultado es la mediana
			xy_r.x = X[CANT/2];
			xy_r.y = Y[CANT/2];


			if (!(xy_r.x== 4095 && xy_r.y==0))
			{
				xQueueSend(qTouch,&xy_r,portMAX_DELAY);
				vTaskDelay(300);
			}

		}
	}
}



extern touch_t mCalib,bCalib;
void calibTouch ()
{
	touch_t xy1,xy2;
	TFT_Black();

	LineaH(80-9,60,19,TFT_Color(255,255,255));
	LineaV(80,60-9,19,TFT_Color(255,255,255));

	xQueueReceive(qTouch,&xy1,portMAX_DELAY);

	LineaH(240-9,180,19,TFT_Color(255,255,255));
	LineaV(240,180-9,19,TFT_Color(255,255,255));
	xQueueReceive(qTouch,&xy2,portMAX_DELAY);



	mCalib.x = (1000*(240-80))/(xy2.x-xy1.x);
	mCalib.y = (1000*(180-60))/(xy2.y-xy1.y);

	bCalib.x= (1000*(80*xy2.x-240*xy1.x))/(xy2.x-xy1.x);
	bCalib.y= (1000*(60*xy2.y-180*xy1.y))/(xy2.y-xy1.y);




}

void Warning(char *p)
{
	int i;
	const int height = HEIGHT_FULL/10+4;
	const int width = W_SENIAL/10*9;

	for (i = 1; i < height-1; i++)	LineaH((W_SENIAL-width)/2, HEIGHT_FULL/2+height/2-i, width, COLOR_T_FONDO); // Item actual

	LineaH((W_SENIAL-width)/2, HEIGHT_FULL/2+height/2-1, width, COLOR_T_LETRA);
	LineaH((W_SENIAL-width)/2, HEIGHT_FULL/2+height/2-height+1, width, COLOR_T_LETRA);

	LineaV((W_SENIAL-width)/2, HEIGHT_FULL/2-height/2+1,height-1,COLOR_T_LETRA);
	LineaV(W_SENIAL-(W_SENIAL-width)/2-1, HEIGHT_FULL/2-height/2+1,height-1,COLOR_T_LETRA);

	TFT_PrintSmall(p,(W_SENIAL-width)/2+4,HEIGHT_FULL/2-height/4,COLOR_T_LETRA,COLOR_T_FONDO);
}


static void guiTask(void * p)
{
	touch_t xy;
	char msj[] = "Save Complete: LPC000.DAT";
	int32_t aux=0;

	calibTouch();

	while (1)
	{
		TFT_Black();
		dibujarBordes();
		dibujarGrilla();
		do {
			if (xSemaphoreTake( semNewImage,0) )
			{
				xSemaphoreTake( semBufferOut,portMAX_DELAY);
				taskSenial();
				xSemaphoreGive( semBufferOut);
			}
		} while(!xQueueReceive(qTouch,&xy,200));

		xy.x = (xy.x*mCalib.x+bCalib.x)/1000;
		xy.y = (xy.y*mCalib.y+bCalib.y)/1000;

		if (xy.x>W_SENIAL && xy.y<H_ITEMS_SENIAL*2 )
		{
			if (usbAttached)
			{
				xSemaphoreTake( semBufferOut,portMAX_DELAY);
				Warning("Saving File");
				aux = USB_WriteFile();
				if (aux<0) Warning("Error");
				else
				{
					msj[20]=aux%10+'0';
					msj[19]=(aux/10)%10+'0';
					msj[18]=(aux/100)%10+'0';
					Warning(msj);
				}
				xSemaphoreGive( semBufferOut);
				vTaskDelay(2000);
			}
			else
			{
				Warning("Usb UnAttached");
				vTaskDelay(2000);
			}
		}
		else  taskMenu();
	}
}




int main(void){
	uint8_t i;
	vSemaphoreCreateBinary(semTouch);
	vSemaphoreCreateBinary(semNewImage);
	vSemaphoreCreateBinary(semBufferOut);
	qTouch = xQueueCreate (1,sizeof(touch_t));
	qDMA = xQueueCreate (1,sizeof(int32_t));
	xTaskCreate(touchTask, (signed const char *)"touchTask", 512, 0, tskIDLE_PRIORITY+5, 0);
	xTaskCreate(guiTask, (signed const char *)"guitask", 512, 0, tskIDLE_PRIORITY+5, 0);
	xTaskCreate(procTask, (signed const char *)"proctask", 512, 0, tskIDLE_PRIORITY+1, 0);

	initHardware();


	vTaskStartScheduler();
	while(1)
	{

	}
}


/** @} doxygen end group definition */
/** @} doxygen end group definition */




/*==================[end of file]============================================*/

/*
 * taskSenial.c
 *
 *  Created on: 3/11/2015
 *      Author: nahuel
 */

//#include "taskSenial.h"
#include "../inc/processingTasks.h"
#include "global.h"
#include "variablesGlobales.h"

int calculux();

const uint8_t * const short_sItems[N_SHORT_ITEMS] = { 	(uint8_t*) "USB      ", (uint8_t*) "NotInUse:", (uint8_t*) "T.Flanco:",
														(uint8_t*) "Flanco:  ", (uint8_t*) "EscV:    ", (uint8_t*) "EscT:    ",
														(uint8_t*) "ValMed:  ", (uint8_t*) "Vpp:     " };




int32_t buff[] = { 2048,2099,2150,2202,2253,2304,2355,2406,2456,2507,2557,2606,2656,2705,2753,2801,2849,2896,2942,2988,3034,3079,3123,3166,3209,3251,3292,3333,3372,3411,3449,3486,3522,3558,3592,3625,3657,3689,3719,3748,3776,3803,3829,3854,3878,3900,3921,3942,3961,3978,3995,4010,4024,4037,4048,4059,4068,4075,4082,4087,4091,4094,4095,4095,4094,4091,4087,4082,4075,4068,4059,4048,4037,4024,4010,3995,3978,3961,3942,3921,3900,3878,3854,3829,3803,3776,3748,3719,3689,3657,3625,3592,3558,3522,3486,3449,3411,3372,3333,3292,3251,3209,3166,3123,3079,3034,2988,2942,2896,2849,2801,2753,2705,2656,2606,2557,2507,2456,2406,2355,2304,2253,2202,2150,2099,2048,1996,1945,1893,1842,1791,1740,1689,1639,1588,1538,1489,1439,1390,1342,1294,1246,1199,1153,1107,1061,1016,972,929,886,844,803,762,723,684,646,609,573,537,503,470,438,406,376,347,319,292,266,241,217,195,174,153,134,117,100,85,71,58,47,36,27,20,13,8,4,1,0,0,1,4,8,13,20,27,36,47,58,71,85,100,117,134,153,174,195,217,241,266,292,319,347,376,406,438,470,503,537,573,609,646,684,723,762,803,844,886,929,972,1016,1061,1107,1153,1199,1246,1294,1342,1390,1439,1489,1538,1588,1639,1689,1740,1791,1842,1893,1945,1996,2047};


void taskSenial(void) {
	int i;

	drawSignal(bufferOut,COLOR_SENIAL,1);
	calculux();

	//forma rápida de borrar:
	TFT_PrintSmall(	"1234567",
							W_SENIAL + BORDER_LINE+MARGIN_X2_T_ITEM_SENIAL,
							6 * H_ITEMS_SENIAL + MARGIN_Y2_T_ITEM_SENIAL,
							COLOR_T_FONDO,COLOR_T_FONDO);
	TFT_PrintSmall(	txtVcc,	W_SENIAL + BORDER_LINE+MARGIN_X2_T_ITEM_SENIAL,
							6 * H_ITEMS_SENIAL + MARGIN_Y2_T_ITEM_SENIAL,
							COLOR_T_LETRA_VALUE,COLOR_T_FONDO);
	TFT_PrintSmall(	"1234567",
								W_SENIAL + BORDER_LINE+MARGIN_X2_T_ITEM_SENIAL,
								7 * H_ITEMS_SENIAL + MARGIN_Y2_T_ITEM_SENIAL,
								COLOR_T_FONDO,COLOR_T_FONDO);
	TFT_PrintSmall(	txtVpp,	W_SENIAL + BORDER_LINE+MARGIN_X2_T_ITEM_SENIAL,
							7 * H_ITEMS_SENIAL + MARGIN_Y2_T_ITEM_SENIAL,
							COLOR_T_LETRA_VALUE,COLOR_T_FONDO);

//	for (i=0;i<250;i++)
//	{
//		buff[i] = 4095 - buff[i];
//	}
//
//	drawSignal(buff,250,COLOR_SENIAL,1);
}

void dibujarBordes(void) {
	uint32_t i, j;

	// Borde divisor grueso
	for (i = 0; i < BORDER_LINE; i++)
		LineaV(W_SENIAL + i, 0, HEIGHT_FULL, COLOR_BORDER_LINE_SENIAL);

	for (i = 2; i < N_ITEMS_SENIAL ; i++) {
		for (j = 0; j < BORDER_LINE; j++)
			LineaH(W_SENIAL, H_ITEMS_SENIAL * i - BORDER_LINE / 2 + j,
			W_IT_SENIAL, COLOR_BORDER_LINE_SENIAL);
	}

	// Impresion del texto USB
		TFT_PrintBig( 	short_sItems[0],
						W_SENIAL+BORDER_LINE+MARGIN_X_T_USB,
						W_SENIAL+BORDER_LINE+MARGIN_Y_T_USB,
						COLOR_T_LETRA_VALUE,COLOR_T_FONDO		) ;


	for (i=2;i<N_ITEMS_SENIAL-4 ;i++){
		// imprime la versión corta del nombre del item (tipo flanco y % flanco)
		TFT_PrintSmall(	short_sItems[i],
						W_SENIAL + BORDER_LINE+MARGIN_X1_T_ITEM_SENIAL,
						i * H_ITEMS_SENIAL + MARGIN_Y1_T_ITEM_SENIAL,
						COLOR_T_LETRA_VALUE,COLOR_T_FONDO );
		//imprime el valor
		TFT_PrintSmall(	pItem_string[i-2][ iItem_value[i-2] ],
						W_SENIAL + BORDER_LINE+MARGIN_X2_T_ITEM_SENIAL,
						i * H_ITEMS_SENIAL + MARGIN_Y2_T_ITEM_SENIAL,
						COLOR_T_LETRA_VALUE,COLOR_T_FONDO);
	}

	// Esc V
	// imprime la versión corta del nombre del item Esc V
	TFT_PrintSmall(	short_sItems[i],
					W_SENIAL + BORDER_LINE+MARGIN_X1_T_ITEM_SENIAL,
					i * H_ITEMS_SENIAL + MARGIN_Y1_T_ITEM_SENIAL,
					COLOR_T_LETRA_VALUE,COLOR_T_FONDO );
	//imprime el valor
	TFT_PrintSmall(	pItem_string[i-2][ iItem_value[i-2] ],
					W_SENIAL + BORDER_LINE+MARGIN_X2_T_ITEM_SENIAL,
					i * H_ITEMS_SENIAL + MARGIN_Y2_T_ITEM_SENIAL,
					COLOR_T_LETRA_VALUE,COLOR_T_FONDO);
	// imprime la unidad

	if ( iItem_value[i-2] <UNIDAD_ESC_V)
		TFT_PrintSmall	(	undiades_V_string2[1] ,
							W_SENIAL + BORDER_LINE+MARGIN_X3_T_ITEM_SENIAL_UNIDAD,
							i * H_ITEMS_SENIAL + MARGIN_Y2_T_ITEM_SENIAL,
							COLOR_T_LETRA_VALUE, COLOR_T_FONDO);
	else
		TFT_PrintSmall	(	undiades_V_string2[0] ,
							W_SENIAL + BORDER_LINE+MARGIN_X3_T_ITEM_SENIAL_UNIDAD,
							i * H_ITEMS_SENIAL + MARGIN_Y2_T_ITEM_SENIAL,
							COLOR_T_LETRA_VALUE, COLOR_T_FONDO);



	i++;

	// EscT
	// imprime la versión corta del nombre del item EscT
	TFT_PrintSmall(	short_sItems[i],
					W_SENIAL + BORDER_LINE+MARGIN_X1_T_ITEM_SENIAL,
					i * H_ITEMS_SENIAL + MARGIN_Y1_T_ITEM_SENIAL,
					COLOR_T_LETRA_VALUE,COLOR_T_FONDO );
	//imprime el valor
	TFT_PrintSmall(	pItem_string[i-2][ iItem_value[i-2] ],
					W_SENIAL + BORDER_LINE+MARGIN_X2_T_ITEM_SENIAL,
					i * H_ITEMS_SENIAL + MARGIN_Y2_T_ITEM_SENIAL,
					COLOR_T_LETRA_VALUE,COLOR_T_FONDO);
	// imprime la unidad


	if ( iItem_value[i-2] < UNIDAD_BASE_TIEMPO)// uS/Div
		TFT_PrintSmall	(	unidades_BT_string2[0] ,
							W_SENIAL + BORDER_LINE+MARGIN_X3_T_ITEM_SENIAL_UNIDAD,
							i * H_ITEMS_SENIAL + MARGIN_Y2_T_ITEM_SENIAL,
							COLOR_T_LETRA_VALUE, COLOR_T_FONDO);
	else // mS/Div
		TFT_PrintSmall	(	unidades_BT_string2[1] ,
							W_SENIAL + BORDER_LINE+MARGIN_X3_T_ITEM_SENIAL_UNIDAD,
							i * H_ITEMS_SENIAL + MARGIN_Y2_T_ITEM_SENIAL,
							COLOR_T_LETRA_VALUE, COLOR_T_FONDO);
	i++;

	// Los valores calculados de Tension media y de Tension pico a pico
	// TODO Reemplazar short_sItems[i] por el string con los valores de Vmed y Vpp
	TFT_PrintSmall(	short_sItems[6],
							W_SENIAL + BORDER_LINE+MARGIN_X1_T_ITEM_SENIAL,
							6 * H_ITEMS_SENIAL + MARGIN_Y1_T_ITEM_SENIAL,
							COLOR_T_LETRA_VALUE,COLOR_T_FONDO );


	TFT_PrintSmall(	short_sItems[7],
								W_SENIAL + BORDER_LINE+MARGIN_X1_T_ITEM_SENIAL,
								7 * H_ITEMS_SENIAL + MARGIN_Y1_T_ITEM_SENIAL,
								COLOR_T_LETRA_VALUE,COLOR_T_FONDO );


//	TFT_PrintSmall(	short_sItems[6],
//					W_SENIAL + BORDER_LINE+MARGIN_X1_T_ITEM_SENIAL,
//					H_ITEM_VMED + MARGIN_Y1_T_ITEM_SENIAL,
//					COLOR_T_LETRA_VALUE,COLOR_T_FONDO );
//	TFT_PrintSmall(	short_sItems[7],
//					W_SENIAL + BORDER_LINE+MARGIN_X1_T_ITEM_SENIAL,
//					H_ITEM_VPP + MARGIN_Y1_T_ITEM_SENIAL,
//					COLOR_T_LETRA_VALUE,COLOR_T_FONDO );
}

void dibujarGrilla(void) {
	uint32_t i, j;

	// Lineas verticales; Calculado para H_DIV_TENSION => PAR
	for (i = 1; i < N_DIV_TIEMPO; i++) {
		if (i == 5) {
			for (j = 0; j < N_DIV_TENSION; j++) {
				set_pixel(i * W_DIV_TIEMPO - 2, j * H_DIV_TENSION, COLOR_GRILLA1);
				set_pixel(i * W_DIV_TIEMPO - 1, j * H_DIV_TENSION, COLOR_GRILLA1);
				set_pixel(i * W_DIV_TIEMPO + 1, j * H_DIV_TENSION, COLOR_GRILLA1);
				set_pixel(i * W_DIV_TIEMPO + 2, j * H_DIV_TENSION, COLOR_GRILLA1);
			}
			LineaV(i * W_DIV_TIEMPO, 0, HEIGHT_FULL, COLOR_GRILLA1);
		} else {
			for (j = 0; j < N_DIV_TENSION * 4; j++)
				set_pixel(i * W_DIV_TIEMPO, j * H_DIV_TENSION / 4,
				COLOR_GRILLA1);
		}
	}

	// Lineas horizontales; Calculado para W_DIV_TIEMPO => IMPAR
	for (i = 1; i < N_DIV_TENSION; i++) {
		if (i == 5) {
			for(j=0;j<N_DIV_TIEMPO;j++){
				set_pixel(j*W_DIV_TIEMPO, i*H_DIV_TENSION-2, COLOR_GRILLA1);
				set_pixel(j*W_DIV_TIEMPO, i*H_DIV_TENSION-1, COLOR_GRILLA1);
				set_pixel(j*W_DIV_TIEMPO, i*H_DIV_TENSION+1, COLOR_GRILLA1);
				set_pixel(j*W_DIV_TIEMPO, i*H_DIV_TENSION+2, COLOR_GRILLA1);
			}
			LineaH(0, i * H_DIV_TENSION, W_SENIAL, COLOR_GRILLA1);
		} else {
			set_pixel(+1, i * H_DIV_TENSION, COLOR_GRILLA1);
			for (j = 0; j < N_DIV_TIEMPO * 4; j++)
				set_pixel(W_DIV_TIEMPO / 4 + 1 + j * W_DIV_TIEMPO / 4,	i * H_DIV_TENSION, COLOR_GRILLA1);
		}
	}

}

/*  *
 * Uso: Delay, solo uso en inicializaciones
 * Dependencias:
 *	- Libreria de SysTick (SysTickConfig[.h][.c])
 * 	- BasicFunctions[.h]
 */

//#include "basicFunctions.h"
//#include "SysTickConfig.h"
#include "TFT.h"

void msDelay(uint32_t delay){
	// SOLO PARA INICIALIZACIONES.
	// NO USAR PARA OTRA COSA!
	uint32_t Demora=10000;

	while(!delay--)
		while(!Demora--);

	return;
}

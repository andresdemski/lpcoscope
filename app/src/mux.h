/*
 * mux.h
 *
 *  Created on: 28/11/2015
 *      Author: ademski
 */

#ifndef APP_INC_MUX_H_
#define APP_INC_MUX_H_

#define MUX_NOT_E		2,8
#define MUX_SEL_B2 		2,10
#define MUX_SEL_B1 		2,11
#define MUX_SEL_B0 		2,12


void changeMux (int i);
void InitMux (void);
//

#endif /* APP_INC_MUX_H_ */

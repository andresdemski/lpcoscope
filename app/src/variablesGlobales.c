/*
 * variablesGlobales.c
 *
 *  Created on: 4/11/2015
 *      Author: nahuel
 */

#include "variablesGlobales.h"
#include "menu.h"
#include "taskSenial.h"
#include "global.h"

const uint8_t * const sItems[N_EQ_ITEMS] = { 	(uint8_t*) "Trigger"	,(uint8_t*) "Tipo de  "	,
												(uint8_t*) "       " 	,(uint8_t*) "Trigger  "	,
												(uint8_t*) "Tension"	,(uint8_t*) "Escala de"	,
												(uint8_t*) "Tiempo " 	,(uint8_t*) "Base de  "	};

//												(uint8_t*) "Escala de"	, (uint8_t*) "Tension"	,
//												(uint8_t*) "Base de  "	, (uint8_t*) "Tiempo " 	,
//												(uint8_t*) "Tipo de  "	, (uint8_t*) "Trigger"	,
//												(uint8_t*) "Trigger  "	, (uint8_t*) "       " 		};

const uint32_t base_tiempo_values 	[N_BASE_TIEMPO]={50,100,250,500,1250,2500,5000,10000,25000,50000,100000};// en us !!!
const uint32_t fs_values 			[N_BASE_TIEMPO]={	FS_MAX,FS_MAX,FS_MAX,FS_MAX,FS_MAX,
														10e4,50e3,25e3,10e3,50e2,
														25e2};// en Hz
const uint32_t esc_v_values			[N_ESC_V]={150,700,1000,2000,3000,4000,5000,6000};//{6000,5000,4000,3000,2000,1000,700,150}; // En mv/div
const uint32_t trigger_values		[N_TRIGGER]={204,409,614,819,1024,1229,1433,1638,1843,2048,2252,2457,2662,2867,3071,3276,3480,3686,3890}; // Valores en porcentaje
const uint32_t trigger_type_values	[N_TRIGGER_TYPE]={0,1};

const uint8_t * base_tiempo_string 	[N_BASE_TIEMPO]={	(uint8_t*) "  5",(uint8_t*) " 10",(uint8_t*) " 25",(uint8_t*) " 50",
														(uint8_t*) "125",(uint8_t*) "250",(uint8_t*) "500",(uint8_t*) "  1",
														(uint8_t*) "2,5",(uint8_t*) "  5",(uint8_t*) " 10"};

// Hasta e incluida la poscion 5, ie. base_tiempo_string[5], los valores son en uS. El resto en mS. Salvo la ultima que esta en Segundos
// Unidades de la base_tiempo
const uint8_t * unidades_BT_string [N_UNIDADES_BT] = { (uint8_t*) "uS/div", (uint8_t*) "mS/div", (uint8_t*) "S/div"};
const uint8_t * unidades_BT_string2 [N_UNIDADES_BT] = { (uint8_t*) "uS/d", (uint8_t*) "mS/d", (uint8_t*) "S/d"};

const uint8_t * esc_v_string 		[N_ESC_V]={(uint8_t*) "150",(uint8_t*) "0,7", (uint8_t*) "  1",(uint8_t*) "  2",(uint8_t*) "  3",(uint8_t*) "  4",(uint8_t*) "  5",(uint8_t*) "  6"};
const uint8_t * undiades_V_string	[N_UNIDADES_V]= {(uint8_t*) "V/div", (uint8_t*) "mV/div"};
const uint8_t * undiades_V_string2	[N_UNIDADES_V]= {(uint8_t*) "V/d", (uint8_t*) "mV/d"};

const uint8_t * trigger_string		[N_TRIGGER]={	(uint8_t*) "-90 %",(uint8_t*) "-80%",(uint8_t*) "-70 %",(uint8_t*) "-60 %",(uint8_t*) "-50 %",(uint8_t*) "-40 %",(uint8_t*) "-30 %",(uint8_t*) "-20 %",(uint8_t*) "-10 %",
													(uint8_t*) "0 %",(uint8_t*) "10 %",(uint8_t*) "20 %",(uint8_t*) "30 %",(uint8_t*) "40 %",(uint8_t*) "50 %",(uint8_t*) "60 %",(uint8_t*) "70 %",(uint8_t*) "80 %",	(uint8_t*) "90 %"		};
const uint8_t * trigger_type_string	[N_TRIGGER_TYPE]={(uint8_t*) "Rising", (uint8_t*)"Falling"};

const uint32_t *pItem_values[N_ITEMS]={		trigger_type_values,
											trigger_values,
											esc_v_values,
											base_tiempo_values}; // puntero a array de valores posibles de un item

const uint8_t **pItem_string[N_ITEMS]={ 	trigger_type_string,
											trigger_string,
											esc_v_string,
											base_tiempo_string};

/* Tiene en cada posicion el indice de otro vector
 * que tiene los valores posibles de cada item,
 * ej c_timebase_values */
uint32_t iItem_value[N_ITEMS]; // indice value Items

/* Tiene en cada posicion la cantidad de valores posibles de cada item*/
uint32_t Item_valueMAX[N_ITEMS]={	N_TRIGGER_TYPE,
									N_TRIGGER,
									N_ESC_V,
									N_BASE_TIEMPO
									 }; // constItem_valueMAX


uint8_t nItem = 0; // Indice del item

uint8_t txtVcc[MAX_LENG_T_ITEMS] = {'1','2','3','4','5','\0'};
uint8_t txtVpp[MAX_LENG_T_ITEMS] = {'2','\0'};

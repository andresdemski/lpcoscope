
#include "board.h"
#include "global.h"
#include "globalRTOS.h"
#include "FreeRTOS.h"
#include "queue.h"

// TODO: add includes adc,dma,lpcopen?




/**	@brief Interrupt handler of DMA
* 	Interrumpe cuando se recibe la cantidad de muestras indicada en SIZE_INTERRUPT_DMA.
* 	Envía por Queue cursor que indica la posición donde comienza el buffer con las muestras.
* 	Indica al DMA que redireccione los nuevos datos en buffer circular para no pisar los anteriores.
*/




#include "../inc/initApp.h"

#include "globalRTOS.h"
#include "global.h"
#include "processingTasks.h"
#include "variablesGlobales.h"
#include "menu.h"
// TODO: includes rtos (tasks, scheduler, ...)

/**	@brief Initializes ADC, DMA. Creates Queues, Semaphores, Tasks
* 	ADC in burst mode with DMA
* 	Queues: qCursor, qTriggerEnable, qEdgePosition, qStartProcessing, qRefreshScreen, qTouch, qTouchCmd
* 	Semaphores: semScreen, semBufferData
* 	Tasks: tSearchTrigger, tCopyBuffer, tProcessData, tDrawSignal, tMenu, tSave2USB, tReadTouchScreen
*/

ADC_CLOCK_SETUP_T ADCSetup;
int initAPP(void){

	/* Carga de valores iniciales */
	/*
	currentConfig.timeBase_us = 50;
	currentConfig.triggerLevel = FULLSCALE_ADC>>1; //by default, half of FULL SCALE
	currentConfig.edgeType = edgeRISE; //by default, Rising edge
	currentConfig.mode = modeCONTINUOUS;
	currentConfig.hist = 0x2; //
	*/

	/* Configuración del ADC en modo burst  */
	/* P0.2 (Función 2), CH7 */

	Chip_IOCON_PinMuxSet(LPC_IOCON, 0, 2, IOCON_FUNC2);
	Chip_ADC_Init(LPC_ADC, &ADCSetup);
	Chip_ADC_SetSampleRate(LPC_ADC, &ADCSetup, fs_values[iBT]);
	Chip_ADC_EnableChannel(LPC_ADC, ADC_CH7, ENABLE);
	Chip_ADC_Int_SetChannelCmd(LPC_ADC,7, ENABLE);
	NVIC_DisableIRQ(ADC_IRQn);
	Chip_ADC_SetBurstCmd(LPC_ADC, ENABLE);

	/* Inicialización del controlador GPDMA */
	Chip_GPDMA_Init(LPC_GPDMA);
	//TODO: ACOMODAR ACTIVACIÓN INTERRUPCIÓN
	NVIC_EnableIRQ(DMA_IRQn); //OJO!!! NO debe ejecutarse interrupción ANTES DEL SCHEDULER
	//dmaChannel = Chip_GPDMA_GetFreeChannel(LPC_GPDMA, GPDMA_CONN_ADC);
	//Chip_GPDMA_Transfer(LPC_GPDMA, dmaChannel, GPDMA_CONN_ADC, (uint)bufferDMA2, GPDMA_TRANSFERTYPE_P2M_CONTROLLER_DMA, SIZE_INTERRUPT_DMA);

	iItem_value[iTriggerType]	= edgeFALL;
	iItem_value[iTriggerValue]	= Trigger_0;
	iItem_value[iScaleV]		= VDIV_6000m;
	iItem_value[iBT]			= BT_25000us;
	SetCurrentConfig();

	return 1; // meh, evitamos warning
}

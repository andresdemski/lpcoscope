/**	@brief Declaration of the tasks that process data in.
 */

#ifndef PROCESSTASKS_H_
#define PROCESSTASKS_H_

#include "stdint.h" //uint32_t,

int32_t searchTrigger ( int32_t cursor, int32_t * trigger);
void procTask (void *p);
void actualizarCantidades();
/**	@brief Signal drawing algorithm
 */
void drawSignal(int *buffer,  uint16_t color, int eraseOld);
void ADC_MaskValuesDMABuff ();
int32_t trigger (int32_t v);

#endif

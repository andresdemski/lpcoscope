/**	@brief Global defines and variables declaration for the scope
 */


#ifndef GLOBAL_H_
#define GLOBAL_H_

#include "stdint.h"
#include "board.h"

/* Defines para manejo DMA*/
#define	SIZE_DMA						750		// Tamaño del buffer circular del DMA
#define	SIZE_INTERRUPT_DMA				250			// Cada cuántas muestras interrumpe el DMA

#define	FULLSCALE_ADC	((int) 0x00000FFF)	// Valor máximo que toma el ADC
#define	ZERO_ADC		((int) FULLSCALE_ADC >> 1)

/* Defines para interpolación */
#define Fc 						20e3							// Frecuencia de corte del filtro
#define ZEROS					3									// Hasta que valor nulo de la sinc sampleamos
#define Wc						((double)2.0*3.1415926535*Fc)
#define SPS						( (int) 200000 )	// Frecuencia de sampling del ADC (200KSPS)

#define	SCREEN_WIDTH	( (int) 250 )			// Ancho de la pantalla [píxels]
#define SCREEN_HEIGHT	( (int) 240 )			//Altura pantalla

#define	LOWER_BT			50								// Base de tiempo [us] más baja (tiempo a mostrar en pantalla)
#define	HIGHER_BT			307200						// Base de tiempo [us] más alta (tiempo a mostrar en pantalla)
#define SIZE_OUT			( 250 )			// Tres veces SCREEN_WIDTH (para poder moverse una pantalla para cada lado)

#define	MAXSINCSIZE		375		// se calculó en función de otros defines. Queda así para evitar cálculos en tiempo de ejecución.
//MAXSINCSIZE = zeros*fs_out_max/2/fcorte

#define COLOR_SIGNAL				((uint16_t)0xFFE0)//TFT_Color(255,255,0)
#define COLOR_FONDO					((uint16_t)0)//TFT_Color(0,0,0)

/// Buffer donde el DMA carga las muestras del adc
extern int32_t bufferDMA[SIZE_DMA];
extern int32_t bufferDMA1[SIZE_DMA];
/// Buffer donde se copian las muestras para procesar (así no son pisadas por el dma)

extern int32_t  bufferOut[SIZE_OUT];
/// Punteros al inicio del buffer que ya está procesado y al que se está procesando respectivamente

/// Number of channel available in dma.
extern int dmaChannel;

/// Estructura con valores de configuración
typedef struct ssConfig{
	int	timeBase_us,
					voltDiv,
					triggerLevel,
					edgeType,
					hist,
					mode,
					timeOffset,
					voltOffset;
} sConfig;


typedef struct ssCantidades{
	int	totalInputSize,			// cantidad TOTAL de MUESTRAS del ADC necesarias (3*SIZE_SCREEN+2*sincEffectiveSize)/upsamplingfactor
					sizeInputBeforeEdge,// cantidad de MUESTRAS del ADC PREVIAS AL FLANCO
					sincEffectiveSize,	// cantidad de coeficientes que se usan (en total es el doble, simetría)
					sincStep,						// cuántos pasos de la sinc hay que saltearse para la convolución (SINC_SIZE / sincEffectiveSize)
					upsamplingFactor,		//
					downsamplingFactor;	//
	//cantUseful;
	// cantOut = 3*SCREEN_WIDTH;
} sCantidades;

extern sConfig currentConfig;
extern sCantidades cantidades;
extern ADC_CLOCK_SETUP_T ADCSetup;

#endif

/*
 * variablesGlobales.h
 *
 *  Created on: 4/11/2015
 *      Author: nahuel
 */

#ifndef APP_INC_VARIABLESGLOBALES_H_
#define APP_INC_VARIABLESGLOBALES_H_

#include <stdint.h>
#include "menu.h"
#include "taskSenial.h"


//#define N_ITEMS 		16 // Declarada en menu.h, pero no la ve
//#define W_SENIAL		250 // Declarada en taskSenial.h, pero no la ve

#define N_BASE_TIEMPO 	11
#define N_ESC_V			8
#define N_TRIGGER		19
#define N_TRIGGER_TYPE	2

#define	N_UNIDADES_BT		3
#define N_UNIDADES_V		2

#define MAX_LENG_T_ITEMS	7

extern uint8_t txtVcc[];
extern uint8_t txtVpp[];


extern const uint8_t * const sItems[N_EQ_ITEMS];

extern const uint32_t base_tiempo_values 	[N_BASE_TIEMPO];
extern const uint32_t esc_v_values			[N_ESC_V];
extern const uint32_t trigger_values		[N_TRIGGER];
extern const uint32_t trigger_type_values	[N_TRIGGER_TYPE];
extern const uint32_t fs_values				[N_BASE_TIEMPO];

extern const uint8_t * base_tiempo_string 	[N_BASE_TIEMPO];
extern const uint8_t * unidades_BT_string 	[N_UNIDADES_BT];
extern const uint8_t * unidades_BT_string2 	[N_UNIDADES_BT];
extern const uint8_t * esc_v_string 		[N_ESC_V];
extern const uint8_t * undiades_V_string	[N_UNIDADES_V];
extern const uint8_t * undiades_V_string2	[N_UNIDADES_V];
extern const uint8_t * trigger_string		[N_TRIGGER];
extern const uint8_t * trigger_type_string	[N_TRIGGER_TYPE];

extern const uint32_t *pItem_values[N_ITEMS]; // puntero a array de valores posibles de un item
extern const uint8_t **pItem_string[N_ITEMS];

extern uint32_t iItem_value[N_ITEMS]; // indice value Itemsr
extern uint32_t Item_valueMAX[N_ITEMS]; // constItem_valueMAX

extern uint8_t nItem; // Indice del item

extern touch_t mCalib; // Touch Calibration
extern touch_t bCalib; // Touch



#endif /* APP_INC_VARIABLESGLOBALES_H_ */

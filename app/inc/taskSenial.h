/*
 * taskSenial.h
 *
 *  Created on: 3/11/2015
 *      Author: nahuel
 */

#ifndef APP_INC_TASKSENIAL_H_
#define APP_INC_TASKSENIAL_H_

/********************************* Includes *****************************************/
#include "board.h"
#include "main.h"
//#include "task.h"
#include "TFT.h"
#include "lineas.h"
#include "menu.h"
/************************************************************************************/

/********************************* Prototipos ***************************************/
void taskSenial(void);
void dibujarBordes(void);
void dibujarGrilla (void);
void dibujarSenial(void);
/************************************************************************************/

/********************************* Defines *****************************************/
#define N_DIV_TIEMPO	10
#define N_DIV_TENSION	10

#define W_DIV_TIEMPO 	W_SENIAL/N_DIV_TIEMPO
#define H_DIV_TENSION	HEIGHT_FULL/N_DIV_TENSION

#define W_SENIAL		250
#define W_IT_SENIAL		WIDTH_FULL-W_SENIAL // WIDTH ITEM SENIAL
#define H_ITEMS_SENIAL	240/N_ITEMS_SENIAL

#define N_ITEMS_SENIAL	8

#define COLOR_BORDER_LINE_SENIAL	TFT_Color(255,255,255)
#define COLOR_GRILLA1				TFT_Color(255,255,255)
#define COLOR_GRILLA2				TFT_Color(224,224,224)
#define COLOR_GRILLA3				TFT_Color(192,192,192)
#define COLOR_BLACK					TFT_Color(0,0,0)
#define COLOR_SENIAL				TFT_Color(255,255,0)


#define MARGIN_X1_T_ITEM_SENIAL			2
#define MARGIN_X2_T_ITEM_SENIAL			8
#define MARGIN_Y1_T_ITEM_SENIAL			14
#define MARGIN_Y2_T_ITEM_SENIAL			3
#define MARGIN_X_T_USB					15
#define MARGIN_Y_T_USB					25
#define MARGIN_X3_T_ITEM_SENIAL_UNIDAD 32

#define N_SHORT_ITEMS		8
/************************************************************************************/


#endif /* APP_INC_TASKSENIAL_H_ */



#ifndef INITAPP_H_
#define INITAPP_H_

/**	@brief Initializes ADC, DMA. Creates Queues, Semaphores, Tasks
 * 	ADC in burst mode with DMA
 * 	Queues: qCursor, qTriggerEnable, qEdgePosition, qStartProcessing, qRefreshScreen, qTouch, qTouchCmd
 * 	Semaphores: semScreen, semBufferData
 * 	Tasks: tSearchTrigger, tCopyBuffer, tProcessData, tDrawSignal, tMenu, tSave2USB, tReadTouchScreen
 */
int initAPP(void);

#endif

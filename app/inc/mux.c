///*
// * mux.c
// *
// *  Created on: 29/11/2015
// *      Author: ademski
// */
//#include "mux.h"
//#include "gpio_17xx_40xx.h"
//
//void InitMux (void)
//{
//	Chip_IOCON_PinMux(LPC_IOCON,MUX_NOT_E,IOCON_MODE_PULLUP,IOCON_FUNC0);
//	Chip_GPIO_SetPinDIROutput(LPC_GPIO,MUX_NOT_E);
//	Chip_IOCON_DisableOD(LPC_IOCON,MUX_NOT_E);
//	GPIO_SetPin_LOW(LPC_GPIO,MUX_NOT_E);
//
//	Chip_IOCON_PinMux(LPC_IOCON,MUX_SEL_B2,IOCON_MODE_PULLUP,IOCON_FUNC0);
//	Chip_GPIO_SetPinDIROutput(LPC_GPIO,MUX_SEL_B2);
//	Chip_IOCON_DisableOD(LPC_IOCON,MUX_SEL_B2);
//	GPIO_SetPin_LOW(LPC_GPIO,MUX_SEL_B2);
//
//	Chip_IOCON_PinMux(LPC_IOCON,MUX_SEL_B1,IOCON_MODE_PULLUP,IOCON_FUNC0);
//	Chip_GPIO_SetPinDIROutput(LPC_GPIO,MUX_SEL_B1);
//	Chip_IOCON_DisableOD(LPC_IOCON,MUX_SEL_B1);
//	GPIO_SetPin_LOW(LPC_GPIO,MUX_SEL_B1);
//
//	Chip_IOCON_PinMux(LPC_IOCON,MUX_SEL_B0,IOCON_MODE_PULLUP,IOCON_FUNC0);
//	Chip_GPIO_SetPinDIROutput(LPC_GPIO,MUX_SEL_B0);
//	Chip_IOCON_DisableOD(LPC_IOCON,MUX_SEL_B0);
//	GPIO_SetPin_LOW(LPC_GPIO,MUX_SEL_B0);
//
//}
//
//
//void changeMux (int i)
//{
//	Chip_GPIO_SetPinState(LPC_IOCON, MUX_SEL_B0, (i>>0)&1);
//	Chip_GPIO_SetPinState(LPC_IOCON, MUX_SEL_B1, (i>>1)&1);
//	Chip_GPIO_SetPinState(LPC_IOCON, MUX_SEL_B2, (i>>2)&1);
//}
//

/**	@brief Global defines and variables declaration (RTOS stuff only)
 */

#ifndef GLOBALRTOS_H_
#define GLOBALRTOS_H_

#include "FreeRTOS.h"
#include "semphr.h"
#include "queue.h"
#include "task.h"

/* Definición de las prioridades */
#define SEARCHTRIGGER_PRIORITY		( tskIDLE_PRIORITY + 8 )
#define COPYBUFFER_PRIORITY				( tskIDLE_PRIORITY + 10 )
#define PROCESSDATA_PRIORITY			( tskIDLE_PRIORITY + 6 )
#define DRAWSIGNAL_PRIORITY				( tskIDLE_PRIORITY + 7 )
#define MENU_PRIORITY							( tskIDLE_PRIORITY + 9 )
#define SAVE2USB_PRIORITY					( tskIDLE_PRIORITY + 9 )
#define READTOUCHSCREEN_PRIORITY	( tskIDLE_PRIORITY + 9 )


/* Declaración de las queues */
/// Queue de tamaño 1 (se pisan valores) que el dma actualiza con el cursor a donde apunta el último buffer cargado.
extern xQueueHandle qDMA;
/// Queue de tamaño 1. La lee searchTrigger().
extern xQueueHandle qTriggerEnable;
/// Queue con posición del flanco encontrado (apunta a dirección de memoria dónde ocurrió el flanco)
extern xQueueHandle qEdgeCursor;
/// Queue que indica que se empiece a interpolar
extern xQueueHandle qStartProcessing;
/// Queue que indica que se refresque la pantalla
extern xQueueHandle qDrawSignal;
/// Queue que avisa que hubo interrupción por touch
//extern xQueueHandle qTouch;
/// Queue que envía el comando que se seleccionó
//extern xQueueHandle qTouchCmd;
//extern xQueueHandle qQUEUE_NAME;

/* Declaración de los semáforos (mutex) */
extern xSemaphoreHandle semNewImage;
extern xSemaphoreHandle semBufferOut;




#endif

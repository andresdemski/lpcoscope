/*
 * menu.h
 *
 *  Created on: 25/10/2015
 *      Author: nahuel
 */
#ifndef APP_INC_MENU_H_
#define APP_INC_MENU_H_
/********************************* Includes *****************************************/
#include "board.h"
#include "main.h"
//#include "task.h"
#include "TFT.h"
#include "lineas.h"
//#include "variablesGlobales.h"
/************************************************************************************/

/********************************* Prototipos ***************************************/
void taskMenu (void);
void dibujarMenu (void);
void selItem(uint8_t d);
uint32_t chValItem(uint32_t a);
void printValItem(void);
void SetCurrentConfig(void);
/************************************************************************************/

/********************************* Defines *****************************************/
/****** Defines de pantalla ****/
// Defines del sector de botones, del de items a seleccionar
// y del sector que muestra el valor del item seleccionado
#define N_ITEMS		4
#define WIDTH_FULL 	320
#define HEIGHT_FULL	240
#define W_ITEM		160   			//Ancho del sector de los items
#define W_BUT		80 				//Ancho del sector de los botones
#define H_ITEM		HEIGHT_FULL/N_ITEMS //Altura de cada item
#define H_BUT_UP	HEIGHT_FULL/3	//Altura del boton 'up'
#define H_BUT_DW	H_BUT_UP
#define H_BUT_IN	HEIGHT_FULL/3	//Altura del boton 'increase'
#define H_BUT_DE	H_BUT_IN		//Altura del boton 'decrease'
#define H_VAL		HEIGHT_FULL/3	//Altura del sector donde se imprime el valor

#define N_EQ_ITEMS	N_ITEMS*2 	// Numero equivalente de items. Utilizado para que el texto entre en el sector de item
#define H_EQ_ITEM	H_ITEM/2	//Altura del texto de los items
#define H_T_VAL		//Altura del texto del valor

#define MARGIN_X_BUT	10// Márgen del boton respecto a la linea
#define MARGIN_Y_BUT	(H_BUT_UP-S_BUT_UP/2)/2

#define MARGIN_X1_T_ITEM_VALUE 	WIDTH_FULL/2+26
#define MARGIN_X2_T_ITEM_VALUE	WIDTH_FULL*3/4
#define MARGIN_X3_T_ITEM_VALUE	WIDTH_FULL/2+35
#define MARGIN_X4_T_ITEM_VALUE	WIDTH_FULL/2+50
#define MARGIN_Y_T_ITEM_VALUE 	HEIGHT_FULL/2-8
#define MARGIN_X1_ESC_V			WIDTH_FULL/2+10
#define MARGIN_X2_ESC_V			WIDTH_FULL*3/4


// Grosores de las lineas
#define BORDER_LINE	3
#define ITEM_LINE	1

// Defines del 'BUTTON UP'
#define S_BUT_UP	61	//Ancho de la base de la flecha, IMPAR
#define PX_BUT_UP	W_ITEM+W_BUT+MARGIN_X_BUT	//Posición X del boton
#define PY_BUT_UP	HEIGHT_FULL-MARGIN_Y_BUT-S_BUT_UP/2+1	//Posicion Y del boton

// Define del 'BUTTON DOWN'
#define S_BUT_DW	61	//Ancho de la base de la flecha, IMPAR
#define PX_BUT_DW	W_ITEM+MARGIN_X_BUT	//Posición X del boton
#define PY_BUT_DW	HEIGHT_FULL-MARGIN_Y_BUT	//Posicion Y del boton

// Define del 'BUTTON +'
#define S1_BUT_IN		61 //Size1: Ancho del boton incrementar ;IMPAR
#define S2_BUT_IN		11 //Size2: Grosor del boton; IMPAR
#define PX1_BUT_IN		PX_BUT_UP // Botones + y up alineados
#define PY1_BUT_IN		H_BUT_IN/2-1+S2_BUT_IN/2
#define PX2_BUT_IN		PX1_BUT_IN+S1_BUT_IN/2-S2_BUT_IN/2
#define PY2_BUT_IN		PY1_BUT_IN-S1_BUT_IN/2-S2_BUT_IN/2

// Define del 'BUTTON -'
#define S1_BUT_DC	61 //Size1: Ancho del boton decrementar, menos
#define S2_BUT_DC	S2_BUT_IN
#define PX_BUT_DC	PX_BUT_DW // Botones - y down alineados
#define PY_BUT_DC	PY1_BUT_IN

// Define de colores
#define COLOR_BORDER_LINE	TFT_Color(255,255,255)
#define COLOR_ITEM_LINE		TFT_Color(255,255,255)
#define COLOR_BUT_UP		TFT_Color(0,255,0)
#define COLOR_BUT_DW		TFT_Color(0,255,0)
#define COLOR_BUT_IN		TFT_Color(0,255,0)
#define COLOR_BUT_DC		TFT_Color(0,255,0)
#define COLOR_T_FONDO		TFT_Color(0,0,0)
#define COLOR_T_LETRA		TFT_Color(255,255,255)
#define COLOR_T_LETRA_VALUE TFT_Color(255,255,255)
//TFT_COLOR: return( ((uint16_t)(r & 0xF8) << 8) | ((uint16_t)(g & 0xFC) << 3) | ((uint16_t)(b & 0xF8) >> 2) );
//#define COLOR_SIGNAL		((uint16_t)0xFFE0);//TFT_Color(255,255,0);
//#define COLOR_FONDO			((uint16_t)0)//TFT_Color(0,0,0)


#define MARGIN_T_LEFT		2
#define MARGIN_T_BOTT_1		9
#define MARGIN_T_BOTT_2		3

#define BUT_UP	0
#define BUT_DW	1
#define BUT_IN	2
#define BUT_DC	3
#define BUT_SAL -1
#define BUT_NOBUT	4

#define UP	BUT_UP
#define DW	BUT_DW

#define UNIDAD_BASE_TIEMPO	7	//Unidad de la base de tiempo. Posicion en el vector a partir de la que cambia de uS a mS
#define UNIDAD_ESC_V		1
/************************************************************************************/
extern touch_t mCalib,bCalib;

#define FS_MAX			100000

enum eItemSelector{iTriggerType, iTriggerValue, iScaleV, iBT};
enum eBT_{BT_50us, BT_100us, BT_250us, BT_500us, BT_1000us, BT_2500us, BT_5000us, BT_10000us,BT_25000us,BT_50000us,BT_100000us,BT_250000us,BT_500000us};
enum eTriggerValue{Trigger_90n,Trigger_80n,Trigger_70n,Trigger_60n,Trigger_50n,Trigger_40n,Trigger_30n,Trigger_20n,Trigger_10n,Trigger_0,Trigger_10p,Trigger_20p,Trigger_30p,Trigger_40p,Trigger_50p,Trigger_60p,Trigger_70p,Trigger_80p,Trigger_90p};
enum eVoltDiv{VDIV_150m,VDIV_700m,VDIV_2000m,VDIV_3000m,VDIV_1000m,VDIV_4000m,VDIV_5000m,VDIV_6000m};//{VDIV_6000m,VDIV_5000m,VDIV_4000m,VDIV_3000m,VDIV_2000m,VDIV_1000m,VDIV_700m,VDIV_150m};
enum eEdgeType{edgeRISE=0,edgeFALL};
enum eLastState{lastLOW=0, lastHIGH, lastDK};
enum eMode{modeCONTINUOUS=0, modeSINGLE};

#endif /* APP_INC_MENU_H_ */
